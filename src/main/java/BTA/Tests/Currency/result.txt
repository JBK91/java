<?xml version="1.0" encoding="UTF-8"?>
<gesmes:Envelope xmlns:gesmes="http://www.gesmes.org/xml/2002-08-01" xmlns="http://www.ecb.int/vocabulary/2002-08-01/eurofxref">
	<gesmes:subject>Reference rates</gesmes:subject>
	<gesmes:Sender>
		<gesmes:name>European Central Bank</gesmes:name>
	</gesmes:Sender>
	<Cube>
		<Cube time='2018-08-30'>
			<Cube currency='USD' rate='1.1692'/>
			<Cube currency='JPY' rate='130.32'/>
			<Cube currency='BGN' rate='1.9558'/>
			<Cube currency='CZK' rate='25.749'/>
			<Cube currency='DKK' rate='7.4568'/>
			<Cube currency='GBP' rate='0.89758'/>
			<Cube currency='HUF' rate='326.32'/>
			<Cube currency='PLN' rate='4.2882'/>
			<Cube currency='RON' rate='4.6437'/>
			<Cube currency='SEK' rate='10.6440'/>
			<Cube currency='CHF' rate='1.1339'/>
			<Cube currency='ISK' rate='125.10'/>
			<Cube currency='NOK' rate='9.7200'/>
			<Cube currency='HRK' rate='7.4405'/>
			<Cube currency='RUB' rate='79.2207'/>
			<Cube currency='TRY' rate='7.8560'/>
			<Cube currency='AUD' rate='1.6015'/>
			<Cube currency='BRL' rate='4.8221'/>
			<Cube currency='CAD' rate='1.5117'/>
			<Cube currency='CNY' rate='7.9887'/>
			<Cube currency='HKD' rate='9.1773'/>
			<Cube currency='IDR' rate='17175.55'/>
			<Cube currency='ILS' rate='4.2220'/>
			<Cube currency='INR' rate='82.7150'/>
			<Cube currency='KRW' rate='1297.53'/>
			<Cube currency='MXN' rate='22.2356'/>
			<Cube currency='MYR' rate='4.8048'/>
			<Cube currency='NZD' rate='1.7534'/>
			<Cube currency='PHP' rate='62.532'/>
			<Cube currency='SGD' rate='1.5968'/>
			<Cube currency='THB' rate='38.239'/>
			<Cube currency='ZAR' rate='17.0100'/>
		</Cube>
	</Cube>
</gesmes:Envelope>
