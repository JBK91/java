package BTA.Tests.Currency;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Main {
  private static String path = "src/BTA/Tests/Currency/result.txt";

  public static void main(String[] args) throws URISyntaxException, MalformedURLException {
    String str = readFromFile();

    writeToFile(str);

    System.out.println(findCurrency("php", str));

    Map<String, Double> map = findAll(str);

    for (String k : map.keySet()) {
      System.out.println(k + " " + map.get(k));
    }
  }

  private static Map<String, Double> findAll(String str) {
    Map<String, Double> map = new HashMap<>();

    String regex = "<Cube currency=\'(\\w{3})\' rate=\'([.\\d]+)\'[/]>";

    Pattern pat = Pattern.compile(regex);

    Matcher m = pat.matcher(str);

    while (m.find()) {
      map.put(m.group(1), Double.parseDouble(m.group(2)));
    }

    return map.entrySet().stream().
            sorted(Comparator.comparing(Map.Entry::getValue)).
            collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> a, LinkedHashMap::new));
  }

  private static String findCurrency(String curr, String str) {
    String regex;

    String result = null;

    regex = "<Cube currency=\'" + curr.toUpperCase() + "\' rate=\'([.\\d]+)\'[/]>";

    Pattern pat = Pattern.compile(regex);


    Matcher m = pat.matcher(str);

    while (m.find()) {
      result = m.group(1);
    }

    if (result == null) {
      result = "Not found";
    }

    return result;
  }

  private static void writeToFile(String str) {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
      writer.write(str);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static String readFromFile() throws URISyntaxException, MalformedURLException {
    URI uri = new URI("http", "www.ecb.europa.eu", "/stats/eurofxref/eurofxref-daily.xml", null);
    URL url = uri.toURL();

    StringBuilder sb = new StringBuilder();

    try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), UTF_8))) {
      String eil;

      while ((eil = reader.readLine()) != null) {
        sb.append(eil).append("\n");
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    return sb.toString();
  }
}
