package BTA.Tests.Differences;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
  public static void main(String[] args) throws MalformedURLException {
    List<String> list = Arrays.stream(args).collect(Collectors.toList());

    URL url1 = new URL(list.get(list.indexOf("-1") + 1));
    URL url2 = new URL(list.get(list.indexOf("-2") + 1));
    String file = list.get(list.indexOf("-f") + 1);

    String text1 = getText(url1);
    String text2 = getText(url2);

    Set<String> set1 = Arrays.stream(text1.split("[\\s.,\"“„()>:?\\d-]+")).
            map(String::toLowerCase).collect(Collectors.toSet());
    Set<String> set2 = Arrays.stream(text2.split("[\\s.,\"“„()>?:\\d-]+")).
            map(String::toLowerCase).collect(Collectors.toSet());

    Set<String> words = set1.stream().filter(x -> !set2.contains(x)).collect(Collectors.toSet());

    writeToFile(words, file);
  }

  private static void writeToFile(Set<String> set, String file) {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
      for (String w : set) {
        writer.write(w);
        writer.newLine();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static String getText(URL url) {
    StringBuilder sb = new StringBuilder();

    try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), StandardCharsets.UTF_8))) {
      String eil;

      while ((eil = reader.readLine()) != null) {
        sb.append(eil).append(" ");
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    Pattern p = Pattern.compile("<body[^>]*>(.+)</body>");

    Matcher m = p.matcher(sb.toString());

    String body = null;

    while (m.find()) {
      body = m.group(1);
    }

    return Objects.requireNonNull(body).replaceAll("<script[^>]*>[^<]+</script>", " ").replaceAll("<[^>]+>", " ");
  }
}