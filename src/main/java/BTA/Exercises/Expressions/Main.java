package BTA.Exercises.Expressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
  public static void main(String[] args) {
    String e = "(3+2.8)/((4.132*5.4) * (2*5))";

    System.out.println(eval(e));
  }

  private static double eval(String e) {
    e = e.replaceAll("\\s*", "");

    while (!e.matches("[\\d.]+")) {
      Pattern p = Pattern.compile("[(][^()]+[)]");

      Matcher m = p.matcher(e);

      String ee = null;

      while (m.find()) {
        ee = m.group();
      }

      if (ee != null) {
        e = e.replace(ee, eval(ee.replace("(", "").replace(")", "")) + "");
      } else {
        ee = null;
        while (e.contains("/")) {
          p = Pattern.compile("([\\d.]+)([/])([\\d.]+)");

          m = p.matcher(e);

          while (m.find()) {
            e = e.replace(m.group(), calculate(m.group(1), m.group(2), m.group(3)));
          }
        }

        while (e.contains("*")) {
          p = Pattern.compile("([\\d.]+)([*])([\\d.]+)");

          m = p.matcher(e);

          while (m.find()) {
            e = e.replace(m.group(), calculate(m.group(1), m.group(2), m.group(3)));
          }
        }

        while (e.contains("-")) {
          p = Pattern.compile("([\\d.]+)([-])([\\d.]+)");

          m = p.matcher(e);

          while (m.find()) {
            e = e.replace(m.group(), calculate(m.group(1), m.group(2), m.group(3)));
          }
        }

        while (e.contains("+")) {
          p = Pattern.compile("([\\d.]+)([+])([\\d.]+)");

          m = p.matcher(e);

          while (m.find()) {
            e = e.replace(m.group(), calculate(m.group(1), m.group(2), m.group(3)));
          }
        }
      }
    }

    return Double.parseDouble(e);
  }

  private static String calculate(String d1, String a, String d2) {
    double n1 = Double.parseDouble(d1);
    double n2 = Double.parseDouble(d2);

    switch (a) {
      case "+":
        return n1 + n2 + "";
      case "-":
        return n1 - n2 + "";
      case "/":
        return n1 / n2 + "";
      case "*":
        return n1 * n2 + "";
    }

    return "0";
  }
}