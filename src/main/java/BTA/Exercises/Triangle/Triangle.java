package BTA.Exercises.Triangle;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Triangle {
  int a, b, c;

  public Triangle(int a, int b, int c) {
    this.a = a;
    this.b = b;
    this.c = c;
  }

  public static void main(String[] args) throws IOException {
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    int a = 0, b = 0, c = 0;

    try {
      System.out.println("Įveskite a kraštinę:");

      a = Integer.parseInt(br.readLine());

      System.out.println("Įveskite b kraštinę:");

      b = Integer.parseInt(br.readLine());

      System.out.println("Įveskite c kraštinę:");

      c = Integer.parseInt(br.readLine());

      Triangle tri = new Triangle(a, b, c);

      tri.sortEdges();

      if (tri.isTriangle()) {
        tri.checkType();
        System.out.println("Jo plotas yra: " + tri.calculateArea());
      } else {
        System.out.println("Tai ne trikampis");
      }
    } catch (NumberFormatException nfe) {
      System.err.println("Invalid Format!");
    }
  }

  public void setEdge(int[] y) {
    this.a = y[0];
    this.b = y[1];
    this.c = y[2];
  }

  public void sortEdges() {
    int[] arr = {this.a, this.b, this.c};

    Arrays.sort(arr);

    setEdge(arr);
  }

  public boolean isTriangle() {
    if (this.c <= this.a + this.b) {
      return true;
    } else {
      return false;
    }
  }

  public void checkType() {
    if (this.a == this.b && this.b == this.c) {
      System.out.println("Trikampis lygiakraštis");
    } else if (this.a == this.b || this.a == this.b || this.b == this.c) {
      System.out.println("Trikampis lygiašionis");
    } else if (this.c == Math.sqrt(this.a * this.a + this.b * this.b)) {
      System.out.println("Trikampis statusis");
    } else {
      System.out.println("Trikampis įvairiakraštis");
    }
  }

  public double calculateArea() {
    int p2 = (this.a + this.b + this.c) / 2;
    double s = Math.sqrt((double) p2 * ((double) p2 - (double) this.a) * ((double) p2 - (double) this.b) * ((double) p2 - (double) this.c));
    return (double) Math.round(s * 100) / 100;
  }
}