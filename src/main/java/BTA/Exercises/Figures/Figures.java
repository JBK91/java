package BTA.Exercises.Figures;

public class Figures {
  public static void main(String[] args) {
    fList<Rectangle> figures = new fList<Rectangle>();

    figures.push(new Rectangle("Black", 5, 3));
    figures.push(new Square("Black", 40));
    figures.push(new Square("White", 3));
    figures.push(new Rectangle("White", 1, 2));

    for (Figure fig : figures.iteratorCustom()) {
      System.out.println(fig);
    }

    for (Figure f : figures) {
      System.out.println(f);
    }
  }
}
