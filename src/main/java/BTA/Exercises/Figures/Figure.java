package BTA.Exercises.Figures;

public abstract class Figure {
  private String color;

  public Figure(String color) {
    this.color = color;
  }

  public abstract int area();

  public String getColor() {
    return color;
  }
}
