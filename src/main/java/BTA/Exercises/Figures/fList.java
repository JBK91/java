package BTA.Exercises.Figures;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class fList<E extends Rectangle> implements Iterable<E> {
  private List<E> list = new ArrayList<>();

  public void push(E el) {
    list.add(el);
  }

  public E show(int ind) {
    if (ind < 0 || ind > list.size()) return null;
    return list.get(ind);
  }

  @Override
  public Iterator<E> iterator() {
    return list.iterator();
  }

  public Iterable<E> iteratorCustom() {
    return new Iterable<E>() {
      @Override
      public Iterator<E> iterator() {
        List<E> sList = new ArrayList<>(list);
        Collections.sort(sList, (E e, E el) -> e.area() - el.area());
        return new CustomIterator<E>(sList);
      }
    };
  }
}