package BTA.Exercises.Figures;

public class Square extends Rectangle {
  public Square(String color, int a) {
    super(color, a, a);
  }

  @Override
  public String toString() {
    return "Square{" + super.area() + "}";
  }
}
