package BTA.Exercises.Figures;

public class Rectangle extends Figure {
  private int a;
  private int b;

  public Rectangle(String color, int a, int b) {
    super(color);
    this.a = a;
    this.b = b;
  }

  public int area() {
    return a * b;
  }

  @Override
  public String toString() {
    return "Rectangle{" + area() + "}";
  }
}
