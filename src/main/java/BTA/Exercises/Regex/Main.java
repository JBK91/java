package BTA.Exercises.Regex;

import org.jsoup.parser.Parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
  public static void main(String[] args) throws MalformedURLException, URISyntaxException {
    String strr = read();

    String str = Parser.parse(strr, "").text();

    System.out.println(str);

    Map<String, Long> strMap = getMap(str);

    for (String k : strMap.keySet()) {
      System.out.println(k + " " + strMap.get(k));
    }
  }

  private static Map<String, Long> getMap(String txt) {
    String reg = "\\W+";

    return Arrays.stream(txt.split(reg)).
            collect(Collectors.groupingBy(String::toLowerCase, Collectors.counting())).entrySet().stream().
            sorted((x, y) -> x.getValue().equals(y.getValue()) ?
                    x.getKey().compareTo(y.getKey()) :
                    y.getValue().compareTo(x.getValue())).
            collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> a, LinkedHashMap::new));
  }

  private static String read() throws URISyntaxException, MalformedURLException {
    URI uri = new URI("https", "en.wikipedia.org", "/w/index.php", "title=Tiger&printable=yes", null);

    URL url = uri.toURL();

    StringBuilder strb = new StringBuilder();

    try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), StandardCharsets.UTF_8))) {
      String e;

      while ((e = reader.readLine()) != null) {
        strb.append(e);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    Pattern pat = Pattern.compile("(<p>?).+(</p>)");

    Matcher m = pat.matcher(strb.toString());

    StringBuilder strbb = new StringBuilder();

    while (m.find()) {
      strbb.append(m.group());
      strbb.append("\n");
    }

    return strbb.toString();
  }
}
