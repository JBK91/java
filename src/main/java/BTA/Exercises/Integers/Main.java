package BTA.Exercises.Integers;

public class Main {
  public static void main(String[] args) {
    int a = 8;
    int b = 10;

    a += b;
    b = a - b;
    a -= b;

    System.out.println("a = " + a);
    System.out.println("b = " + b);
  }
}
