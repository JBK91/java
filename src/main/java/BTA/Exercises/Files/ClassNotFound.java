package BTA.Exercises.Files;

public class ClassNotFound extends Exception {
  private Exception e;

  public ClassNotFound(String message, Exception e) {
    super(message);
    this.e = e;
  }

  @Override
  public String toString() {
    return getMessage();
  }
}
