package BTA.Exercises.Files;

public class FileNotFound extends Exception {
  private Exception e;

  public FileNotFound(String message, Exception e) {
    super(message);
    this.e = e;
  }

  @Override
  public String toString() {
    return getMessage();
  }
}
