package BTA.Exercises.Files;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Files {
  public static void main(String[] args) {
    List<Student> students = new ArrayList<>();

    students.add(new Student("Justinas", "Karaliunas", 1));
    students.add(new Student("Darius", "Navickas", 1));
    students.add(new Student("Lukas", "Scerbinskas", 2));
    students.add(new Student("Modestas", "Mikelionis", 3));
    students.add(new Student("Petras", "Mikelionis", 2));
    students.add(new Student("Katazyna", "Balcevic", 3));

    if ("save".equals(args[0])) {
      writeFile(students);
    }

    if ("restore".equals(args[0])) {
      readFile();
    }
  }

  private static void readFile() {
    try (ObjectInput objInput = new ObjectInputStream(
            new BufferedInputStream(
                    new FileInputStream("src/BTA/Exercises/Files/ObjectFile.txt")))) {
      Object object = objInput.readObject();

      if (object instanceof List) {
        List<Student> students = (List<Student>) object;

        for (Student stu : students) {
          System.out.println(stu);
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(new FileNotFound("File Not Found", e));
    } catch (ClassNotFoundException e) {
      throw new RuntimeException(new ClassNotFound("Class Not Found", e));
    }
  }

  private static void writeFile(List<Student> students) {
    try (ObjectOutput objOutput = new ObjectOutputStream(
            new BufferedOutputStream(
                    new FileOutputStream("src/BTA/Exercises/Files/ObjectFile.txt")))) {
      objOutput.writeObject(students);
      objOutput.writeObject(students.toString());

      System.out.println("Object written in file");
    } catch (IOException e) {
      System.out.println("Labasggggg");
      throw new RuntimeException(new FileNotFound("File Not Found", e));
    }
  }
}
