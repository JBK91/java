package BTA.Exercises.Files;

public class Student extends Person {
  private int classRoom;

  public Student(String firstName, String lastName, int classRoom) {
    super(firstName, lastName);
    this.classRoom = classRoom;
  }

  public int getClassRoom() {
    return classRoom;
  }

  @Override
  public String toString() {
    return "Student{" +
            "firstName = " + super.getFirstName() +
            ", lastName = " + super.getLastName() +
            ", classRoom = " + classRoom +
            '}';
  }
}
