package BTA.Exercises.Shop;

import java.math.BigDecimal;

public class Shop {
  private static void getSaleInfo(int quantity, BigDecimal sum) {
    BigDecimal valueWithoutPVM = sum.multiply(new BigDecimal(79)).divide(new BigDecimal(100));
    BigDecimal quantityValueWithoutPVM = valueWithoutPVM.divide(new BigDecimal(quantity));
    BigDecimal pvm = sum.subtract(valueWithoutPVM);

    System.out.println("Goods value: " + quantityValueWithoutPVM);
    System.out.println("Quantity: " + quantity);
    System.out.println("Sum without PVM: " + String.format("%.2f", valueWithoutPVM));
    System.out.println("PVM: " + String.format("%.2f", pvm));
    System.out.println("Sum with PVM: " + sum);
  }

  public static void main(String[] args) {
    BigDecimal sum = BigDecimal.valueOf(2034, 2);
    getSaleInfo(5, sum);
  }
}
