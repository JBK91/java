package BTA.Exercises.PerfectNumber;

public class PerfectNumber {

  int[] arr;

  public static int findDividersSum(int sk) {
    int sum = 0;

    for (int i = 1; i < sk; ++i) {
      if (sk % i == 0) {
        sum += i;
      }
    }

    return sum;
  }

  public static void main(String[] args) {
    System.out.println("Tobuli skaičiai nuo 1 iki 1000:");

    for (int i = 1; i <= 1000; ++i) {
      int sk = findDividersSum(i);

      if (sk == i) {
        System.out.println(i);
      }
    }
  }
}