package BTA.Exercises.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.logging.*;

public class Main {
  public static void main(String[] args) throws IOException {

    Logger logger = Logger.getLogger(Main.class.getName());
    logger.setLevel(Level.ALL);

    Handler h = new FileHandler("src/BTA/Exercises/Logger/abc.log", true);

    h.setFormatter(new Formatter() {

      @Override
      public String format(LogRecord record) {
        Gson g = new GsonBuilder().setPrettyPrinting().create();
        return g.toJson(record);
      }
    });

    logger.addHandler(h);

    logger.severe("Klaida");
    logger.warning("Įspėjimas");
    logger.info("Pranešimas");
    logger.config("Konfigūravimas");
    logger.fine("Detalus");
    logger.finer("Detalesnis");
    logger.finest("Detaliausias");

    logger.log(Level.WARNING, "Kitas Įspėjimas");
  }
}
