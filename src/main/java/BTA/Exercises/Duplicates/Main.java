package BTA.Exercises.Duplicates;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
  private static String path = "C:/Users/JBKJu/Desktop";
  private static String output = "src/main/java/BTA/Exercises/Duplicates/output.txt";

  public static void main(String[] args) throws InterruptedException {
    File file = new File(path);

    Map<String, File> fileList = Collections.synchronizedMap(new HashMap<>());
    Set<File> fileSet = Collections.synchronizedSet(new HashSet<>());

    getFiles(fileList, fileSet, file);

    List<File> lol = fileSet.stream().sorted(Comparator.comparing(File::getName)).
            collect(Collectors.toList());

    writeToFile(lol);
  }

  private static void writeToFile(List<File> fileSet) {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(output))) {
      for (File f : fileSet) {
        writer.write(f.getPath());
        writer.newLine();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void getFiles(Map<String, File> fileList, Set<File> fileSet, File file) throws InterruptedException {
    File[] files = file.listFiles();

    for (File f : Objects.requireNonNull(files)) {
      if (f.isDirectory()) {
        Thread t = new Thread(() -> {
          try {
            getFiles(fileList, fileSet, f);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        });
        System.out.println(t.getName());
        t.start();
        t.join();
      } else {
        if (fileList.containsKey(f.getName())) {
          fileSet.add(f);
          fileSet.add(fileList.get(f.getName()));
        } else {
          fileList.put(f.getName(), f);
        }
      }
    }
  }
}