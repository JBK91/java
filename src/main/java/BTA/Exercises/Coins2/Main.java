package BTA.Exercises.Coins2;

public class Main {
  public static void main(String[] args) {
    int sk = 4;

    long time = System.currentTimeMillis();

    System.out.print(sk + ": ");

    while (sk != 0) {
      int re = getCoin(sk);
      System.out.print(re == sk ? re : re + " + ");
      sk -= re;
    }

    System.out.println("\n" + (System.currentTimeMillis() - time));
  }

  private static int getCoin(int sk) {
    for (int i = sk; i > 0; i--) {
      if (isPrimary(i)) {
        if (sk - i == 1) continue;
        return i;
      }
    }

    return 2;
  }

  private static boolean isPrimary(int sk) {
    for (int i = 2; i < sk; ++i) {
      if (sk % i == 0) return false;
    }

    return true;
  }
}