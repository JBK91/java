package BTA.Exercises.Threads;

import java.util.stream.Collectors;

public class Main {
  public static void main(String[] args) throws InterruptedException {
    RandomGenerator rg = new RandomGenerator();

    Thread t1 = new Thread(() -> {
      for (int i = 0; i < 100; ++i) {
        rg.addRandom();
      }
    });

    Thread t2 = new Thread(() -> {
      for (int i = 0; i < 100; ++i) {
        rg.addRandom();
      }
    });

    Thread t3 = new Thread(() -> {
      for (int i = 0; i < 100; ++i) {
        rg.addRandom();
      }
    });

    Thread t4 = new Thread(() -> {
      for (int i = 0; i < 100; ++i) {
        rg.addRandom();
      }
    });

    Thread t5 = new Thread(() -> {
      for (int i = 0; i < 100; ++i) {
        rg.addRandom();
      }
    });

    t1.start();
    t2.start();
    t3.start();
    t4.start();
    t5.start();

    t1.join();
    t2.join();
    t3.join();
    t4.join();
    t5.join();

    rg.getList().stream().collect(Collectors.groupingBy(x -> x, Collectors.counting())).
            entrySet().forEach(System.out::println);
  }
}