package BTA.Exercises.Threads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class RandomGenerator {
  private List<Integer> list;

  public RandomGenerator() {
    list = Collections.synchronizedList(new ArrayList<>());
  }

  public void addRandom() {
    Random random = new Random();
    list.add(random.nextInt(100) + 1);
  }

  public List<Integer> getList() {
    return list;
  }
}