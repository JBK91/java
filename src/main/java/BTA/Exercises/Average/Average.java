package BTA.Exercises.Average;

public class Average {
  public static double findAverage(int[] arr) {
    int sum = 0;

    for (int i = 0; i < arr.length; ++i) {
      sum += arr[i];
    }

    return (double) sum / arr.length;
  }

  public static void main(String[] args) {
    int[] arr1 = {5, 6, 10, 15, 8, 4};
    int[] arr2 = {8, 5, 3};

    double arr1Average = findAverage(arr1);
    double arr2Average = findAverage(arr2);

    System.out.println("Pirmo masyvo elementų vidurkis yra: " + arr1Average);
    System.out.println("Antro masyvo elementų vidurkis yra: " + arr2Average);
    System.out.println("Abiejų masyvų vidurkių skirtumas yra : " + (arr1Average - arr2Average));
  }
}