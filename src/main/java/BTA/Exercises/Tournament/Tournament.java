package BTA.Exercises.Tournament;

import java.util.ArrayList;
import java.util.Random;

public class Tournament {
  static ArrayList<Participant> participants = new ArrayList<Participant>();
  static ArrayList<Pair> pairs = new ArrayList<Pair>();
  static Random random = new Random();
  static Participant tempParticipant;

  public static ArrayList<Pair> getPairs() {
    ArrayList<Pair> pairs = new ArrayList<Pair>();

    while (participants.size() > 0) {
      int randomIndex = random.nextInt(participants.size());

      Participant temp = participants.get(randomIndex);

      participants.remove(randomIndex);

      randomIndex = random.nextInt(participants.size());

      pairs.add(new Pair(temp, participants.get(randomIndex)));

      participants.remove(randomIndex);
    }

    return pairs;
  }

  public static ArrayList<Participant> getWinners() {
    ArrayList<Participant> participants = new ArrayList<Participant>();

    int randomIndex = random.nextInt(2);

    while (pairs.size() > 0) {
      if ((randomIndex > 0)) {
        participants.add(pairs.get(0).participant2);
      } else {
        participants.add(pairs.get(0).participant1);
      }

      pairs.remove(0);
    }

    return participants;
  }

  static int getRounds() {
    int rounds = 0;
    int size = participants.size();

    while (size > 1) {
      size /= 2;
      rounds++;
    }

    return rounds;
  }

  public static void main(String[] args) {
    participants.add(new Participant("Justinas", "Karaliunas", 1));
    participants.add(new Participant("Darius", "Navickas", 2));
    participants.add(new Participant("Lukas", "Scerbinskas", 3));
    participants.add(new Participant("Modestas", "Mikelionis", 4));
    participants.add(new Participant("Katazyna", "Balsevic", 5));
    participants.add(new Participant("Petras", "Petraitis", 6));
    participants.add(new Participant("Jonas", "Jonaitis", 7));
    participants.add(new Participant("Ona", "Onaite", 8));
    participants.add(new Participant("Jurga", "Jurgita", 9));
    participants.add(new Participant("Virga", "Virgute", 10));
    participants.add(new Participant("Aleksas", "Aleksandras", 11));

    int round = 1;
    String roundName;

    while (getRounds() > 0) {
      if (participants.size() <= 2) {
        System.out.println("Finalas:");
        roundName = "Turnyro laimėtojas:";
      } else if (participants.size() <= 4) {
        System.out.println("Pusfinaliai:");
        roundName = "Pusfinalių laimėtojai:";
      } else if (participants.size() <= 8) {
        System.out.println("Ketvirtfinaliai:");
        roundName = "Ketvirtfinalių laimėtojai:";
      } else if (participants.size() <= 16) {
        System.out.println("Aštunfinaliai:");
        roundName = "Aštunfinalių laimėtojai:";
      } else {
        System.out.println("Raundas: " + round);
        roundName = "Raundo " + round + " laimėtojai:";
      }

      System.out.println("-----");

      if (participants.size() % 2 != 0) {
        int randomIndex = random.nextInt(participants.size());

        tempParticipant = participants.get(randomIndex);

        participants.remove(randomIndex);
      } else {
        tempParticipant = null;
      }

      pairs = getPairs();

      for (Pair pair : pairs) {
        System.out.println(pair.participant1.firstName + " vs " + pair.participant2.firstName);
      }

      System.out.println("-----");

      System.out.println(roundName);

      System.out.println("-----");

      participants = getWinners();

      if (tempParticipant != null) {
        participants.add(tempParticipant);
      }

      for (Participant participant : participants) {
        System.out.println(participant.firstName);
      }

      round++;

      System.out.println("-----");
    }
  }
}