package BTA.Exercises.Tournament;

public class Participant extends Person {
  int ID;

  public Participant(String firstName, String lastName, int ID) {
    super(firstName, lastName);
    this.ID = ID;
  }

  public String getFullName() {
    return this.firstName + " " + this.lastName;
  }
}
