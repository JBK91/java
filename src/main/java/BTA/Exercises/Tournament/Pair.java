package BTA.Exercises.Tournament;

public class Pair {
  Participant participant1;
  Participant participant2;

  public Pair(Participant participant1, Participant participant2) {
    this.participant1 = participant1;
    this.participant2 = participant2;
  }
}
