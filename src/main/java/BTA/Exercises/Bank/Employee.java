package BTA.Exercises.Bank;

public class Employee implements Payment {
  private String bankAccount;
  private String accountOwner;
  private double amount;

  public Employee(String bankAccount, String accountOwner, double amount) {
    this.bankAccount = bankAccount;
    this.accountOwner = accountOwner;
    this.amount = amount;
  }

  @Override
  public String getBankAccount() {
    return bankAccount;
  }

  public void setBankAccount(String bankAccount) {
    this.bankAccount = bankAccount;
  }

  @Override
  public String getAccountOwner() {
    return accountOwner;
  }

  public void setAccountOwner(String accountOwner) {
    this.accountOwner = accountOwner;
  }

  @Override
  public double getAmount() {
    return amount;
  }

  public void setAmount(double amount) {
    this.amount = amount;
  }
}
