package BTA.Exercises.Bank;

public class Client implements Payment {
  private String accountOwner;
  private String bankAccount;
  private double amount;
  private int buffer = 2;
  private double[] amounts;
  private int payCount;

  Client(String accountOwner, String bankAccount, double amount) {
    this.accountOwner = accountOwner;
    this.bankAccount = bankAccount;
    this.amount = amount;
    this.amounts = new double[buffer];
    this.addAmount(amount);
  }

  double average() {
    double sum = 0;

    for (double amo : this.getAmounts()) {
      sum += amo;
    }

    int fullSlots = 0;

    for (double slots : this.getAmounts()) {
      if (slots != 0) fullSlots++;
    }

    return sum / fullSlots;
  }

  void addAmount(double amo) {
    int fullSlots = 0;

    for (double slots : this.getAmounts()) {
      if (slots != 0) fullSlots++;
    }

    if (fullSlots >= this.getBuffer()) {
      this.setBuffer(buffer + 2);

      double[] temp = new double[this.getBuffer()];

      System.arraycopy(this.getAmounts(), 0, temp, 0, this.getAmounts().length);

      this.setAmounts(temp);
    }

    boolean isNull = false;
    int i = 0;

    while (!isNull) {
      if (this.getAmounts()[i] == 0) {
        this.getAmounts()[i] = amo;
        this.setPayCount(this.getPayCount() + 1);
        isNull = true;
      }

      i++;
    }
  }

  int getPayCount() {
    return payCount;
  }

  private void setPayCount(int payCount) {
    this.payCount = payCount;
  }

  private int getBuffer() {
    return buffer;
  }

  private void setBuffer(int buffer) {
    this.buffer = buffer;
  }

  double[] getAmounts() {
    return amounts;
  }

  private void setAmounts(double[] amounts) {
    this.amounts = amounts;
  }

  @Override
  public String getAccountOwner() {
    return accountOwner;
  }

  @Override
  public String getBankAccount() {
    return bankAccount;
  }

  @Override
  public double getAmount() {
    return amount;
  }
}
