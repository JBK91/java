package BTA.Exercises.Bank;

public class Bank {
  private static String findMax(Client[] clients) {
    int max = 0;
    Client maxOwner = null;

    for (Client c : clients) {
      if (c.getPayCount() > max) {
        max = c.getPayCount();
        maxOwner = c;
      }
    }

    return maxOwner.getAccountOwner();
  }

  private static void doublePay(Client[] clients) {
    for (Client c : clients) {
      double ave = c.average();

      for (double amo : c.getAmounts()) {
        if (amo > ave * 2) {
          System.out.println(c.getAccountOwner());
        }
      }
    }
  }

  public static void main(String[] args) {
    Client c1 = new Client("Justinas1", "LT1", 500.1);
    Client c2 = new Client("Justinas2", "LT2", 500.2);
    Client c3 = new Client("Justinas3", "LT3", 500.3);
    Client c4 = new Client("Justinas4", "LT4", 500.4);

    Client[] clients = {c1, c2, c3, c4};

    for (Client c : clients) {
      c.addAmount(100.5);
      c.addAmount(155.3);
    }

    clients[0].addAmount(200.3);

    System.out.println("Daugiausiai mokėjimų atliko: " + findMax(clients));

    System.out.println("Klientai, kurių bent vienas mokėjimas didesnis už 2 vidurkius:");

    doublePay(clients);
  }
}
