package BTA.Exercises.Bank;

public interface Payment {
  String getBankAccount();

  String getAccountOwner();

  double getAmount();
}
