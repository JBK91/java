package BTA.Exercises.Sells;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Sells {

  private static void showMostEarners(Document[] documents) {
    for (int i = 0; i < documents.length; ++i) {
      for (int j = 0; j < documents.length - 1; ++j) {
        if (documents[i].calculateAllEarnings().compareTo(documents[j].calculateAllEarnings()) > 0) {
          Document temp = documents[i];
          documents[i] = documents[j];
          documents[j] = temp;
        }
      }
    }

    System.out.println("Best clients:");

    for (int i = 0; i < 2; ++i) {
      System.out.println(documents[i].getClient() + " - " + String.format("%.2f", documents[i].calculateAllEarnings()));
    }

    System.out.println("\nWorst clients:");

    int index = 0;

    while (index < 2) {
      System.out.println(documents[documents.length - 1 - index].getClient() + " - " + String.format("%.2f", documents[documents.length - 1 - index].calculateAllEarnings()));
      index++;
    }
  }

  private static void showMostSoldGoods(Document[] documents) {
    Map<String, Integer> soldItems = new HashMap<>();

    for (Document doc : documents) {
      for (Good g : doc.getGoods()) {
        if (soldItems.containsKey(g.getGoodName())) {
          soldItems.put(g.getGoodName(), soldItems.get(g.getGoodName()) + g.getQuantity());
        } else {
          soldItems.put(g.getGoodName(), g.getQuantity());
        }
      }
    }

    Stream<Map.Entry<String, Integer>> sortedSoldItems = soldItems.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()));

    AtomicInteger index = new AtomicInteger(0);

    System.out.println("\nMost sold items:");

    sortedSoldItems.forEach((v) -> {
      if (index.get() < 5) {
        System.out.println(v);
      }

      index.getAndIncrement();
    });
  }

  private static void showBiggestProfits(Document[] documents) {
    Map<String, BigDecimal> biggestProfits = new HashMap<>();
    Map<String, BigDecimal> expenses = new HashMap<>();
    Map<String, BigDecimal> biggestProfitsPercentages = new HashMap<>();

    for (Document doc : documents) {
      for (Good g : doc.getGoods()) {
        if (biggestProfits.containsKey(g.getGoodName())) {
          biggestProfits.put(g.getGoodName(), biggestProfits.get(g.getGoodName()).add(g.calculateEarnings()));
        } else {
          biggestProfits.put(g.getGoodName(), g.calculateEarnings());
        }

        if (expenses.containsKey(g.getGoodName())) {
          expenses.put(g.getGoodName(), expenses.get(g.getGoodName()).add(g.getCostPrice()));
        } else {
          expenses.put(g.getGoodName(), g.getCostPrice());
        }
      }
    }

    expenses.forEach((k, v) -> {
      BigDecimal profit = biggestProfits.get(k).multiply(new BigDecimal(100));
      profit = profit.divide(v, MathContext.DECIMAL128);
      profit = profit.setScale(2, BigDecimal.ROUND_HALF_EVEN);
      biggestProfitsPercentages.put(k, profit);
    });

    Stream<Map.Entry<String, BigDecimal>> sortedProfits = biggestProfitsPercentages.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue()));

    System.out.println("\nBiggest profits percentage:");

    AtomicInteger index = new AtomicInteger(0);

    sortedProfits.forEach((v) -> {
      if (index.get() < 5) {
        System.out.println(v);
      }

      index.getAndIncrement();
    });
  }

  private static void showPVMByRate(Document[] documents) {
    Map<Integer, BigDecimal> PVM = new HashMap<>();

    for (Document doc : documents) {
      for (Good g : doc.getGoods()) {
        if (PVM.containsKey(g.getPVM())) {
          PVM.put(g.getPVM(), g.calculateEarnings().add(PVM.get(g.getPVM())));
        } else {
          PVM.put(g.getPVM(), g.calculateAllPVM());
        }
      }
    }

    System.out.println("\nPVM:");

    PVM.forEach((k, v) -> System.out.println(k + " - " + String.format("%.2f", v)));
  }

  public static void main(String[] args) {
    Good[] clientOneGoods = {
            new Good("1234", "Milk", 21, new BigDecimal("0.89"), 10, 5, new BigDecimal("0.20")),
            new Good("12345", "Yogurt", 21, new BigDecimal("1.01"), 15, 20, new BigDecimal("0.33")),
            new Good("1478", "Kent Silver", 9, new BigDecimal("3.21"), 100, 0, new BigDecimal("2.51")),
            new Good("14789", "Beer", 9, new BigDecimal("2.35"), 50, 0, new BigDecimal("1.21")),
            new Good("5896", "Bike", 21, new BigDecimal("399.99"), 2, 15, new BigDecimal("299.99"))
    };

    Good[] clientTwoGoods = {
            new Good("1234", "Milk", 21, new BigDecimal("0.89"), 15, 5, new BigDecimal("0.20")),
            new Good("4777", "Meat", 21, new BigDecimal("2.89"), 78, 30, new BigDecimal("1.55")),
            new Good("9969", "Winston Blue", 9, new BigDecimal("3.15"), 40, 0, new BigDecimal("2.02")),
            new Good("44458", "Lemonade", 21, new BigDecimal("1.95"), 147, 40, new BigDecimal("0.85")),
            new Good("47855", "Bed", 12, new BigDecimal("159.99"), 4, 15, new BigDecimal("99.99"))
    };

    Good[] clientThreeGoods = {
            new Good("9658", "Sunglasses", 21, new BigDecimal("5.87"), 48, 18, new BigDecimal("2.40")),
            new Good("12345", "Yogurt", 21, new BigDecimal("1.01"), 55, 20, new BigDecimal("0.33")),
            new Good("20584", "Laptop", 35, new BigDecimal("859.79"), 3, 50, new BigDecimal("145.05")),
            new Good("14789", "Beer", 9, new BigDecimal("2.35"), 500, 0, new BigDecimal("1.21")),
            new Good("458888", "Biscuits", 21, new BigDecimal("2.58"), 205, 10, new BigDecimal("0.99"))
    };

    Good[] clientFourGoods = {
            new Good("9658", "Sunglasses", 21, new BigDecimal("5.87"), 48, 18, new BigDecimal("2.40")),
            new Good("54321", "Fish", 21, new BigDecimal("12.15"), 28, 36, new BigDecimal("6.34")),
            new Good("20584", "Laptop", 35, new BigDecimal("859.79"), 1, 50, new BigDecimal("145.05")),
            new Good("14789", "Beer", 9, new BigDecimal("2.35"), 1, 0, new BigDecimal("1.21")),
            new Good("55612", "Table", 21, new BigDecimal("59.62"), 11, 15, new BigDecimal("45.23"))
    };

    Good[] clientFiveGoods = {
            new Good("965801", "Chair", 21, new BigDecimal("25.78"), 1, 45, new BigDecimal("12.87")),
            new Good("12345", "Yogurt", 21, new BigDecimal("1.01"), 55, 20, new BigDecimal("0.33")),
            new Good("20584", "Laptop", 35, new BigDecimal("859.79"), 3, 50, new BigDecimal("145.05")),
            new Good("14789", "Beer", 9, new BigDecimal("2.35"), 500, 0, new BigDecimal("1.21")),
            new Good("458888", "Biscuits", 21, new BigDecimal("2.58"), 205, 10, new BigDecimal("0.99"))
    };

    List<Document> documents = new ArrayList<>();
    documents.add(new Document("2018-05-08", "LT1", "Justinas", clientOneGoods));
    documents.add(new Document("2018-07-08", "LT2", "Darius", clientTwoGoods));
    documents.add(new Document("2018-03-08", "LT3", "Lukas", clientThreeGoods));
    documents.add(new Document("2018-04-30", "LT4", "Modestas", clientFourGoods));
    documents.add(new Document("2018-08-06", "LT5", "Petras", clientFiveGoods));

    List<Document> docSorted = documents.stream().sorted((e, el) -> el.calculateAllEarnings().compareTo(e.calculateAllEarnings())).limit(2).collect(Collectors.toList());

    for (Document doc : docSorted) {
      System.out.println(doc.getClient() + " - " + doc.calculateAllEarnings());
    }

    System.out.println("-----");

    Map<String, Item> lol = documents.stream().flatMap(x -> Arrays.stream(x.getGoods()))
            .map(y -> new Item(y.getGoodName(), y.getQuantity(), 1))
            .collect(Collectors.toMap(k -> k.getItemName(), v -> v,
                    (a, b) -> new Item(a.getItemName(),
                            a.getItemQuantity() + b.getItemQuantity(),
                            a.getItemCount() + b.getItemCount()))).entrySet().stream()
            .sorted((e, el) -> el.getValue().getItemQuantity() - e.getValue().getItemQuantity()).limit(5)
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> a, LinkedHashMap::new));

    for (String l : lol.keySet()) {
      System.out.println(l + " - " + lol.get(l).getItemQuantity() + " - " + lol.get(l).getItemCount());
    }

    System.out.println("-----");

    Map<String, BigDecimal> val = documents.stream().flatMap(x -> Arrays.stream(x.getGoods()))
            .collect(Collectors.toMap(g -> g.getGoodName(), v -> v.calculateEarnings()
                    .multiply(BigDecimal.valueOf(100))
                    .divide(v.getCostPrice(), MathContext.DECIMAL128)
                    .setScale(2, BigDecimal.ROUND_HALF_EVEN), BigDecimal::add))
            .entrySet().stream().sorted((e, el) -> el.getValue().compareTo(e.getValue())).limit(5)
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> a, LinkedHashMap::new));

    for (String v : val.keySet()) {
      System.out.println(v + " - " + val.get(v));
    }

    System.out.println("-----");

    Map<Integer, BigDecimal> pvm = documents.stream().flatMap(x -> Arrays.stream(x.getGoods()))
            .collect(Collectors.toMap(x -> x.getPVM(), y -> y.calculateAllPVM(), BigDecimal::add));

    for (Integer p : pvm.keySet()) {
      System.out.println(p + " - " + pvm.get(p));
    }
  }
}

class Item {
  private String itemName;
  private int itemQuantity;
  private int itemCount;

  public Item(String itemName, int itemQuantity, int itemCount) {
    this.itemName = itemName;
    this.itemQuantity = itemQuantity;
    this.itemCount = itemCount;
  }

  public String getItemName() {
    return itemName;
  }

  public int getItemQuantity() {
    return itemQuantity;
  }

  public int getItemCount() {
    return itemCount;
  }
}