package BTA.Exercises.Sells;

import java.math.BigDecimal;

public class Document {
  private String documentDate;
  private String documentNumber;
  private String client;
  private Good[] goods;

  public Document(String documentDate, String documentNumber, String client, Good[] goods) {
    this.documentDate = documentDate;
    this.documentNumber = documentNumber;
    this.client = client;
    this.goods = goods;
  }

  public BigDecimal calculateAllEarnings() {
    BigDecimal result = new BigDecimal(0);

    for (Good good : this.getGoods()) {
      result = result.add(good.calculateEarnings());
    }

    return result;
  }

  public String getDocumentDate() {
    return documentDate;
  }

  public void setDocumentDate(String documentDate) {
    this.documentDate = documentDate;
  }

  public String getDocumentNumber() {
    return documentNumber;
  }

  public void setDocumentNumber(String documentNumber) {
    this.documentNumber = documentNumber;
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }

  public Good[] getGoods() {
    return goods;
  }

  public void setGoods(Good[] goods) {
    this.goods = goods;
  }
}
