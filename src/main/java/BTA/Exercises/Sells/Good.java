package BTA.Exercises.Sells;

import java.math.BigDecimal;

public class Good {
  private String barCode;
  private String goodName;
  private int PVM;
  private BigDecimal priceWithoutPVM;
  private int quantity;
  private int discount;
  private BigDecimal costPrice;

  public Good(String barCode, String goodName, int PVM, BigDecimal priceWithoutPVM, int quantity, int discount, BigDecimal costPrice) {
    this.barCode = barCode;
    this.goodName = goodName;
    this.PVM = PVM;
    this.priceWithoutPVM = priceWithoutPVM;
    this.quantity = quantity;
    this.discount = discount;
    this.costPrice = costPrice.multiply(new BigDecimal(quantity));
  }

  public BigDecimal calculateEarnings() {
    return this.calculatePriceWithPVM().multiply(new BigDecimal(this.getQuantity())).subtract(this.getCostPrice());
  }

  public BigDecimal calculatePriceWithPVM() {
    return this.calculatePriceWithDiscount().multiply(new BigDecimal(this.getPVM() + 100)).divide(new BigDecimal("100"));
  }

  public BigDecimal calculatePriceWithDiscount() {
    return this.getPriceWithoutPVM().multiply(new BigDecimal(100 - this.getDiscount())).divide(new BigDecimal("100"));
  }

  public BigDecimal calculatePVM() {
    return this.getPriceWithoutPVM().multiply(new BigDecimal(this.getPVM())).divide(new BigDecimal("100"));
  }

  public BigDecimal calculateAllPVM() {
    return this.calculatePVM().multiply(new BigDecimal(this.getQuantity()));
  }

  public String getBarCode() {
    return barCode;
  }

  public void setBarCode(String barCode) {
    this.barCode = barCode;
  }

  public String getGoodName() {
    return goodName;
  }

  public void setGoodName(String goodName) {
    this.goodName = goodName;
  }

  public int getPVM() {
    return PVM;
  }

  public void setPVM(int PVM) {
    this.PVM = PVM;
  }

  public BigDecimal getPriceWithoutPVM() {
    return priceWithoutPVM;
  }

  public void setPriceWithoutPVM(BigDecimal priceWithoutPVM) {
    this.priceWithoutPVM = priceWithoutPVM;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public int getDiscount() {
    return discount;
  }

  public void setDiscount(int discount) {
    this.discount = discount;
  }

  public BigDecimal getCostPrice() {
    return costPrice;
  }

  public void setCostPrice(BigDecimal costPrice) {
    this.costPrice = costPrice;
  }
}
