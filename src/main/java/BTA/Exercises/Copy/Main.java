package BTA.Exercises.Copy;

import java.io.*;

public class Main {
  public static void main(String[] args) throws IOException {
    String source = args[0];
    String dest = args[1];

    File sourceDir = new File(source);
    File destDir = new File(dest);

    copyFiles(sourceDir, destDir);
  }

  private static void copyFiles(File source, File dest) throws IOException {
    if (source.isDirectory()) {
      if (!source.exists()) source.mkdirs();

      String[] files = source.list();

      if (files != null) {
        for (String file : files) {
          File srcFile = new File(source, file);
          File destFile = new File(dest, file);

          new Thread(() -> {
            try {
              copyFiles(srcFile, destFile);
            } catch (IOException e) {
              e.printStackTrace();
            }
          }).start();
        }
      }
    } else {
      try (BufferedInputStream in = new BufferedInputStream(new FileInputStream(source));
           BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(dest))) {
        byte[] buffer = new byte[1024];

        int size;

        while ((size = in.read(buffer)) > 0) {
          out.write(buffer, 0, size);
        }
      }
    }
  }
}