package BTA.Exercises.Cars;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Cars implements Comparator<Cars> {
  Owner owner;
  private String number;
  private String model;

  public Cars(String number, String model, Owner owner) {
    this.number = number;
    this.model = model;
    this.owner = owner;
  }

  public static void main(String[] args) {
    List<Cars> cars = new ArrayList<Cars>();

    Cars car1 = new Cars("ABC-123", "Porshe", new Owner("Justinas", "Karaliunas"));
    Cars car2 = new Cars("ZRT-548", "Ferrari", new Owner("Darius", "Navickas"));
    Cars car3 = new Cars("PRT-845", "BMW", new Owner("Lukas", "Scerbinskas"));
    Cars car4 = new Cars("BGT-584", "Mercedez Benz", new Owner("Modestas", "Mikelionis"));
    Cars car5 = new Cars("AAR-015", "Audi", new Owner("Petras", "Petraitis"));
    Cars car6 = new Cars("BUT548", "Beetle", new Owner("Katazina", "Balsevic"));

    cars.add(car1);
    cars.add(car2);
    cars.add(car3);
    cars.add(car4);
    cars.add(car5);
    cars.add(car6);

    Collections.sort(cars, new Comparator<Cars>() {
      @Override
      public int compare(Cars o1, Cars o2) {
        return o1.getOwner().getLastName().compareTo(o2.getOwner().getLastName());
      }
    });

    for (Cars car : cars) {
      System.out.println(car.getOwner().getLastName());
    }

    Collections.sort(cars, new Comparator<Cars>() {
      @Override
      public int compare(Cars o1, Cars o2) {
        return o1.getOwner().getFirstName().compareTo(o2.getOwner().getFirstName());
      }
    });

    for (Cars car : cars) {
      System.out.println(car.getOwner().getFirstName());
    }

    Collections.sort(cars, new Comparator<Cars>() {
      @Override
      public int compare(Cars o1, Cars o2) {
        return o1.getNumber().compareTo(o2.getNumber());
      }
    });

    for (Cars car : cars) {
      System.out.println(car.getNumber());
    }
  }

  public String getNumber() {
    return number;
  }

  public String getModel() {
    return model;
  }

  public Owner getOwner() {
    return owner;
  }

  @Override
  public int compare(Cars o1, Cars o2) {
    return o1.getOwner().getLastName().compareTo(o2.getOwner().getLastName());
  }

  private static class Owner {
    private String firstName;
    private String lastName;

    public Owner(String firstName, String lastName) {
      this.firstName = firstName;
      this.lastName = lastName;
    }

    public String getFirstName() {
      return firstName;
    }

    public String getLastName() {
      return lastName;
    }
  }
}