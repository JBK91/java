package BTA.Exercises.Numbers;

import com.ibm.icu.text.NumberFormat;
import com.ibm.icu.text.RuleBasedNumberFormat;
import com.ibm.icu.util.ULocale;


public class Main {
  public static void main(String[] args) {
    String locale = args[0];

    for (int i = 1; i < args.length; ++i) {
      double value = Double.parseDouble(args[i]);
      print(value, locale);
    }
  }

  private static void print(double value, String loc) {
    ULocale locale = new ULocale(loc);

    NumberFormat f = new RuleBasedNumberFormat(locale, RuleBasedNumberFormat.SPELLOUT);

    System.out.println(f.format(value));
  }
}
