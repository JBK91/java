package BTA.Exercises.Array;


public class Array {
  static int[] arr = {-10, 0, 3, 9, -5};

  public static void main(String[] args) {
    for (int i = 0; i < arr.length; i++) {
      System.out.println(arr[i]);
    }

    for (int i = 0; i < arr.length; i++) {
      for (int j = 0; j < arr.length; j++) {
        if (arr[i] > arr[j]) {
          int temp = arr[i];
          arr[i] = arr[j];
          arr[j] = temp;
        }
      }
    }

    System.out.println("Sorted array elements: ");

    for (int i = 0; i < arr.length; i++) {
      System.out.println(arr[i]);
    }
  }
}