package BTA.Exercises.Date;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Date {
  public static void main(String[] args) {
    List<Document> docs = new ArrayList<Document>();

    docs.add(new Document(LocalDate.of(2018, 1, 3), "Justinas", new BigDecimal("200.05")));
    docs.add(new Document(LocalDate.of(2018, 2, 3), "Justinas", new BigDecimal("150.86")));
    docs.add(new Document(LocalDate.of(2018, 4, 3), "Justinas", new BigDecimal("20.05")));
    docs.add(new Document(LocalDate.of(2018, 5, 3), "Justinas", new BigDecimal("350.99")));
    docs.add(new Document(LocalDate.of(2018, 8, 3), "Justinas", new BigDecimal("255.05")));
    docs.add(new Document(LocalDate.of(2018, 9, 3), "Justinas", new BigDecimal("980.05")));
    docs.add(new Document(LocalDate.of(2018, 10, 3), "Justinas", new BigDecimal("144.05")));
    docs.add(new Document(LocalDate.of(2018, 11, 3), "Justinas", new BigDecimal("100.20")));
    docs.add(new Document(LocalDate.of(2017, 12, 3), "Justinas", new BigDecimal("100.05")));

    Map<Integer, Map<Integer, BigDecimal>> lol = docs.stream()
            .collect(Collectors.groupingBy(x -> x.getDate().getYear(),
                    Collectors.toMap(f -> f.getDate().getMonth().getValue() < 4 ? 1 :
                                    f.getDate().getMonth().getValue() < 7 ? 2 :
                                            f.getDate().getMonth().getValue() < 10 ? 3 : 4,
                            v -> v.getSum(), (a, b) -> a.add(b))));

    System.out.println(lol);
  }
}
