package BTA.Exercises.Date;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Document {
  private LocalDate date;
  private String client;
  private BigDecimal sum;

  public Document(LocalDate date, String client, BigDecimal sum) {
    this.date = date;
    this.client = client;
    this.sum = sum;
  }

  public LocalDate getDate() {
    return date;
  }

  public String getClient() {
    return client;
  }

  public BigDecimal getSum() {
    return sum;
  }
}
