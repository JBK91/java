package BTA.Exercises.Words;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
  private static Set<String> urlSet = new HashSet<>();
  private static Set<String> newSet = new HashSet<>();

  public static void main(String[] args) throws MalformedURLException {
    System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");

    URL url = new URL("https://www.delfi.lt/");

    Set<String> urls = getUrls(url);

    List<String> urlList = checkUrls(urls);

    addUrls(urlList);

    for (String ur : urlSet) {
      URL urll = new URL(ur);

      Set<String> urlss = getUrls(urll);

      List<String> urlsList = checkUrls(urlss);

      newSet.addAll(urlsList);
    }

    System.out.println(newSet.size());
  }

  private static void addUrls(List<String> list) {
    newSet.addAll(list);
    urlSet.addAll(list);
  }

  private static List<String> checkUrls(Set<String> set) {
    List<String> list = new ArrayList<>(set);

    for (int i = 0; i < list.size(); ++i) {
      list.set(i, list.get(i).replaceAll("^[/]{2}.*", ""));
      if (list.get(i).matches("^[/]{1}.*")) list.set(i, "https://www.delfi.lt" + list.get(i));
    }

    return list.stream().filter(x -> x.matches("^https://www[.]delfi[.]lt.*")).collect(Collectors.toList());
  }

  private static Set<String> getUrls(URL url) {
    Set<String> urlList = new HashSet<>();
    String text = getText(url);

    Pattern p = Pattern.compile("<a [^>]*href=\"([^\"]+)\"");

    Matcher m = p.matcher(text);

    while (m.find()) urlList.add(m.group(1));

    return urlList;
  }

  private static String getText(URL url) {
    StringBuilder sb = new StringBuilder();

    try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), StandardCharsets.UTF_8))) {
      String eil;

      while ((eil = reader.readLine()) != null) {
        sb.append(eil).append("\n");
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    return sb.toString();
  }
}
