package BTA.Exercises.Generics;

import java.util.Iterator;
import java.util.List;

public class ReverseIterator<E> implements Iterator<E> {
  private List<E> cars;
  private int current;

  public ReverseIterator(List<E> cars) {
    this.cars = cars;
    this.current = cars.size();
  }

  @Override
  public boolean hasNext() {
    return current > 0;
  }

  @Override
  public E next() {
    return cars.get(--current);
  }
}
