package BTA.Exercises.Generics;

public class Car {
  private String owner;
  private String model;

  public Car(String owner, String model) {
    this.owner = owner;
    this.model = model;
  }

  public String getOwner() {
    return owner;
  }

  public String getModel() {
    return model;
  }

  @Override
  public String toString() {
    return "Car{" + "owner='" + owner + '\'' + ", model='" + model + '\'' + '}';
  }
}
