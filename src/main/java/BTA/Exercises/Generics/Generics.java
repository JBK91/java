package BTA.Exercises.Generics;

import java.util.Iterator;
import java.util.function.Predicate;

public class Generics {
  public static void main(String[] args) {
    eList<Car> cars = new eList<Car>();

    cars.push(new Car("Justinas", "Porshe"));
    cars.push(new Car("Darius", "Oka"));
    cars.push(new Car("Lukas", "Tesla"));
    cars.push(new Car("Modestas", "Audi"));
    cars.push(new Car("Petras", "Lada"));
    cars.push(new Car("Katazina", "Mercedez Benz"));

    System.out.println(cars.count());

    Iterator<Car> carIterator = cars.iterator();

    while (carIterator.hasNext()) {
      Car el = carIterator.next();

      System.out.println(el);
    }

    System.out.println("-----");

    for (Car car : cars.iteratorReverse()) {
      System.out.println(car);
    }

    System.out.println("-----");

    for (Car car : cars.iteratorFilter(new Predicate<Car>() {
      @Override
      public boolean test(Car car) {
        return car.getOwner().equals("Petras");
      }
    })) {
      System.out.println(car);
    }
  }
}
