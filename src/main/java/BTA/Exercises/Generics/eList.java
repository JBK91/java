package BTA.Exercises.Generics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

public class eList<E> implements Iterable<E> {
  private List<E> list = new ArrayList<>();

  public void push(E el) {
    list.add(el);
  }

  public E show(int ind) {
    if (ind < 0 || ind > list.size()) return null;
    return list.get(ind);
  }


  public int count() {
    return list.size();
  }

  @Override
  public Iterator<E> iterator() {
    return list.iterator();
  }

  public Iterable<E> iteratorReverse() {
    return new Iterable<E>() {
      @Override
      public Iterator<E> iterator() {
        return new ReverseIterator<E>(list);
      }
    };
  }

  public Iterable<E> iteratorFilter(Predicate<E> filt) {
    return new Iterable<E>() {
      @Override
      public Iterator<E> iterator() {
        return new FiltIterator<E>(list, filt);
      }
    };
  }
}
