package BTA.Exercises.Generics;

import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

public class FiltIterator<E> implements Iterator<E> {
  private List<E> cars;
  private int current;
  private Predicate<E> filt;

  public FiltIterator(List<E> cars, Predicate<E> filt) {
    this.cars = cars;
    this.current = -1;
    this.filt = filt;
  }

  private int filter(List<E> cars, Predicate<E> filt) {
    if (filt == null) return -1;

    for (int i = current + 1; i < cars.size(); ++i) {
      if (filt.test(cars.get(i))) return i;
    }

    return -1;
  }

  @Override
  public boolean hasNext() {
    current = filter(cars, filt);

    return current > -1;
  }

  @Override
  public E next() {
    return cars.get(current);
  }
}
