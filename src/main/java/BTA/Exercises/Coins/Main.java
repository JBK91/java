package BTA.Exercises.Coins;

public class Main {
  public static void main(String[] args) {
    int sk = 31;

    System.out.print(sk + ": ");

    while (sk != 0) {
      int re = getCoin(sk);
      System.out.print(sk == re ? re : re + " + ");
      sk -= re;
    }
  }

  private static int getCoin(int sk) {
    double rez = 0.0;
    int i = 0;

    while (rez <= sk) {
      rez = Math.pow(2, ++i);
    }

    return (int) Math.pow(2, i - 1);
  }
}