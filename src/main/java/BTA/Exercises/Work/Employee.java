package BTA.Exercises.Work;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Employee {
  Address address;
  private String name;

  public Employee(String name, Address address) {
    this.name = name;
    this.address = address;
  }

  public static void add(Map<String, List<Employee>> ems, Employee e) {
    List<Employee> el = ems.get(e.getAddress().getCity());

    if (el == null) {
      el = new ArrayList<>();
      ems.put(e.getAddress().getCity(), el);
    }

    el.add(e);
  }

  public static void main(String[] args) {
    Employee e1 = new Employee("Justinas", new Address("Joniskis", "Joniskis"));
    Employee e2 = new Employee("Darius", new Address("Jonava", "Jonava"));
    Employee e3 = new Employee("Lukas", new Address("Jonava", "Jonava"));
    Employee e4 = new Employee("Modestas", new Address("Jonava", "Jonava"));
    Employee e5 = new Employee("Petras", new Address("Kaunas", "Kaunas"));
    Employee e6 = new Employee("Katazina", new Address("Vilnius", "Vilnius"));

    Map<String, List<Employee>> emp = new HashMap<>();

    add(emp, e1);
    add(emp, e2);
    add(emp, e3);
    add(emp, e4);
    add(emp, e5);
    add(emp, e6);

    emp.forEach((k, v) -> {
      System.out.println(k + " - " + v.size());
    });
  }

  public String getName() {
    return name;
  }

  public Address getAddress() {
    return address;
  }

  public static class Address {
    private String city;
    private String address;

    public Address(String city, String address) {
      this.city = city;
      this.address = address;
    }

    public String getCity() {
      return city;
    }

    public String getAddress() {
      return address;
    }
  }
}
