package BTA.Exercises.Logs;

public class LogCount {
  private int accessCount;
  private int uniqueUrlCount;

  public LogCount(int accessCount, int uniqueUrlCount) {
    this.accessCount = accessCount;
    this.uniqueUrlCount = uniqueUrlCount;
  }

  public int getAccessCount() {
    return accessCount;
  }

  public int getUniqueUrlCount() {
    return uniqueUrlCount;
  }
}
