package BTA.Exercises.Logs;

import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Main {
  private static String logsPath = "src/BTA/Exercises/Logs/access_logs.txt";
  private static String clientPath = "src/BTA/Exercises/Logs/clients.txt";
  private static String urlPath = "src/BTA/Exercises/Logs/urls.txt";
  private static String timePath = "src/BTA/Exercises/Logs/times.txt";

  public static void main(String[] args) {
    List<Log> logs = readFromFile();

    Map<String, Long> clientMap = logs.stream().
            collect(Collectors.groupingBy(Log::getClient, Collectors.counting())).entrySet().stream().
            sorted(Collections.reverseOrder(Comparator.comparing(Map.Entry::getValue))).
            collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> a, LinkedHashMap::new));

    Map<String, Long> urlMap = logs.stream().
            collect(Collectors.groupingBy(Log::getUrl, Collectors.counting())).entrySet().stream().
            sorted(Collections.reverseOrder(Comparator.comparing(Map.Entry::getValue))).
            collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> a, LinkedHashMap::new));

    writeToFile(clientMap, clientPath);
    writeToFile(urlMap, urlPath);

    Map<String, LogCount> timeMap = logs.stream().
            collect(Collectors.groupingBy(Log::makeKey, Collectors.toList())).entrySet().stream().
            collect(Collectors.toMap(Map.Entry::getKey, x -> new LogCount(x.getValue().size(),
                    x.getValue().stream().collect(Collectors.groupingBy(Log::getUrl, Collectors.counting())).size()))).
            entrySet().stream().sorted(Comparator.comparing(Map.Entry::getKey)).
            collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> a, LinkedHashMap::new));

    writeToFile(timeMap, timePath, 0);
  }

  private static void writeToFile(Map<String, LogCount> map, String path, int l) {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
      for (String k : map.keySet()) {
        writer.write(k + " " + map.get(k).getAccessCount() + " " + map.get(k).getUniqueUrlCount());
        writer.newLine();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void writeToFile(Map<String, Long> map, String path) {
    try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
      for (String k : map.keySet()) {
        writer.write(k + " " + map.get(k));
        writer.newLine();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static List<Log> readFromFile() {
    List<Log> logs = new ArrayList<>();
    String regex = "^([\\d\\w.-]+) - - \\[([\\d\\w:/]+) -\\d+] \"\\w* ?([^ \"]+)";

    Pattern p = Pattern.compile(regex);

    Matcher m;

    try (BufferedReader reader = new BufferedReader(new FileReader(logsPath))) {
      String eil;

      DateTimeFormatter dFormat = DateTimeFormatter.ofPattern("dd/MMM/yyyy:HH:mm:ss");

      while ((eil = reader.readLine()) != null) {
        m = p.matcher(eil);

        while (m.find()) {
          logs.add(new Log(m.group(1), LocalDateTime.parse(m.group(2), dFormat), m.group(3)));
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }

    return logs;
  }
}