package BTA.Exercises.Logs;

import java.time.LocalDateTime;

public class Log {
  private String client;
  private LocalDateTime date;
  private String url;

  public Log(String client, LocalDateTime date, String url) {
    this.client = client;
    this.date = date;
    this.url = url;
  }

  public String makeKey() {
    return date.getYear() + "-" +
            (date.getMonthValue() < 10 ? "0" + date.getMonthValue() : date.getMonthValue()) +
            "-" + (date.getDayOfMonth() < 10 ? "0" + date.getDayOfMonth() : date.getDayOfMonth()) +
            ":" + (date.getHour() < 10 ? "0" + date.getHour() : date.getHour());
  }

  public String getClient() {
    return client;
  }

  public LocalDateTime getDate() {
    return date;
  }

  public String getUrl() {
    return url;
  }

  @Override
  public String toString() {
    return client + " " + date + " " + url;
  }
}
