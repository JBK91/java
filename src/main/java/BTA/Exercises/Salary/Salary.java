package BTA.Exercises.Salary;

public class Salary {
  Employee employee;
  private double salary;

  public Salary(Employee employee, double salary) {
    this.employee = employee;
    this.salary = salary;
  }

  public Employee getEmployee() {
    return employee;
  }

  public double getSalary() {
    return salary;
  }
}
