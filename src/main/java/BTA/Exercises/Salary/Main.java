package BTA.Exercises.Salary;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
  public static void main(String[] args) {
    List<Salary> emps = new ArrayList<Salary>();

    emps.add(new Salary(new Employee("Justinas", "Karaliunas"), 500.0));
    emps.add(new Salary(new Employee("Darius", "Navickas"), 600.0));
    emps.add(new Salary(new Employee("Lukas", "Scerbinskas"), 400.0));
    emps.add(new Salary(new Employee("Modestas", "Mikelionis"), 200.0));
    emps.add(new Salary(new Employee("Katazyna", "Balcevic"), 1500.0));
    emps.add(new Salary(new Employee("Petras", "Jasiulionis"), 400.0));
    emps.add(new Salary(new Employee("Lukas", "Scerbinskas"), 4500.0));
    emps.add(new Salary(new Employee("Lukas", "Scerbinskas"), 100.0));

    List<Salary> empsSorted = emps.stream().sorted((x, y) -> Double.compare(y.getSalary(), x.getSalary())).collect(Collectors.toList());

    for (Salary emp : empsSorted) {
      System.out.println(emp.getEmployee().getFirstName() + " " + emp.getSalary());
    }

    Map<String, Long> salaryCount = emps.stream().collect(Collectors.groupingBy((Salary x) -> x.getEmployee().getFirstName(), Collectors.counting()));

    System.out.println(salaryCount);
  }
}
