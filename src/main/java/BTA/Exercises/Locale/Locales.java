package BTA.Exercises.Locale;

import java.text.ChoiceFormat;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class Locales {
  public static void main(String[] args) {
    printMessage();

    Locale.setDefault(Locale.forLanguageTag("LTU"));

    printMessage();
  }

  private static void printMessage() {
    ResourceBundle messages = ResourceBundle.getBundle(Locales.class.getPackage().getName() + ".ResourceBundle.ResourceBundle", Locale.getDefault());

    String message = messages.getString("message");

    String[] things = messages.getString("thing").split(":");

    String[] bel = messages.getString("breaks").split(":");

    double[] breaks = new double[bel.length];

    for (int i = 0; i < bel.length; ++i) {
      breaks[i] = Double.parseDouble(bel[i]);
    }

    ChoiceFormat cFormat = new ChoiceFormat(breaks, things);

    MessageFormat msgFormat = new MessageFormat(message);

    msgFormat.setFormatByArgumentIndex(0, cFormat);

    for (int i = 1; i < 20; ++i) {
      String res = msgFormat.format(new Object[]{i});
      System.out.println(res);
    }
  }
}
