package BTA.Exercises.Container;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class iteratorCustom<E> implements Iterator<E> {
  List<E> list;
  Comparator<E> cmp;
  int current;

  public iteratorCustom(List<E> list, Comparator<E> cmp) {
    this.list = list;
    this.cmp = cmp;
    this.current = 0;
  }

  @Override
  public boolean hasNext() {
    return current < list.size();
  }

  @Override
  public E next() {
    return list.get(current++);
  }
}
