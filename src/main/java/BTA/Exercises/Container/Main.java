package BTA.Exercises.Container;

public class Main {
  public static void main(String[] args) {
    Container<Employee> emps = new Container<Employee>();

    emps.push(new Employee("Justinas", "Karaliunas", 26));
    emps.push(new Employee("Darius", "Navickas", 24));
    emps.push(new Employee("Lukas", "Screbinskas", 24));
    emps.push(new Employee("Petras", "Jasionis", 31));
    emps.push(new Employee("Modestas", "Mikelionis", 23));
    emps.push(new Employee("Katazyna", "Balcevic", 22));

    for (Employee emp : emps.order((e1, e2) -> e1.getFirstName().compareTo(e2.getFirstName()))) {
      System.out.println(emp);
    }

    System.out.println("-----");

    for (Employee emp : emps) {
      System.out.println(emp);
    }
  }
}
