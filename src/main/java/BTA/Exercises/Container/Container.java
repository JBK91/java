package BTA.Exercises.Container;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class Container<E> implements Iterable<E> {
  private List<E> list = new ArrayList<>();

  public void push(E e) {
    list.add(e);
  }

  @Override
  public Iterator<E> iterator() {
    return list.iterator();
  }

  public Iterable<E> order(Comparator<E> cmp) {
    List<E> sList = new ArrayList<>(list);
    sList.sort(cmp);
    return () -> new iteratorCustom<E>(sList, cmp);
  }
}
