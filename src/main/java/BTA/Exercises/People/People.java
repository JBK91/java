package BTA.Exercises.People;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

public class People {
  public static void main(String[] args) {
    Map<String, Person> peoples = new HashMap<String, Person>();

    Person p1 = new Person("Justias", "Karaliunas", "333");
    Person p2 = new Person("Darius", "Navickas", "355");
    Person p3 = new Person("Lukas", "Scerbinskas", "311");
    Person p4 = new Person("Modestas", "Mikelionis", "344");
    Person p5 = new Person("Petras", "Petraitis", "322");
    Person p6 = new Person("Katazina", "Balsevic", "366");

    peoples.put(p1.getPid(), p1);
    peoples.put(p2.getPid(), p2);
    peoples.put(p3.getPid(), p3);
    peoples.put(p4.getPid(), p4);
    peoples.put(p5.getPid(), p5);
    peoples.put(p6.getPid(), p6);

    Stream<Map.Entry<String, Person>> sortedPeople = peoples.entrySet().stream().sorted(Map.Entry.comparingByKey());

    sortedPeople.forEach((v) -> System.out.println(v.getValue().getFullName()));
  }
}
