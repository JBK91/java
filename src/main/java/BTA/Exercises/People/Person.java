package BTA.Exercises.People;

public class Person {
  private String firstName;
  private String lastName;
  private String pid;

  public Person(String firstName, String lastName, String pid) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.pid = pid;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPid() {
    return pid;
  }

  public void setPid(String pid) {
    this.pid = pid;
  }

  public String getFullName() {
    return this.firstName + " " + this.lastName;
  }
}
