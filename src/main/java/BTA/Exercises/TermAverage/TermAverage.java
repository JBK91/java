package BTA.Exercises.TermAverage;

public class TermAverage {
  public static void main(String[] args) {
    int[] term = {8, 9, 8, 10};

    Student student = new Student("Justinas", "Karaliūnas", term);

    double termAverage = student.calculateTermAverage();

    System.out.println("Student term average is: " + termAverage);
  }
}