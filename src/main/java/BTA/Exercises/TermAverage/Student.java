package BTA.Exercises.TermAverage;

public class Student extends Person {
  int[] term;

  public Student(String firstName, String lastName, int[] term) {
    super(firstName, lastName);
    this.term = term;
  }

  public double calculateTermAverage() {
    int sum = 0;

    for (int e : this.term) {
      sum += e;
    }

    return (double) sum / (double) this.term.length;
  }
}