package BTA.Exercises.Classes;

public class Student extends Person {
  int classRoom;

  public Student(String firstName, String lastName, int classRoom) {
    super(firstName, lastName);
    this.classRoom = classRoom;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public int getClassRoom() {
    return this.classRoom;
  }

  public String toString() {
    return "Student{" + this.firstName + ", " + this.lastName + ", " + this.classRoom + "}";
  }
}