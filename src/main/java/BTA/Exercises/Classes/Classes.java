package BTA.Exercises.Classes;

import java.util.Arrays;
import java.util.Comparator;

public class Classes {
  public static void main(String[] args) {
    Student[] students = {
            new Student("Lukas", "Scerbinskas", 3),
            new Student("Modestas", "Mikelionis", 2),
            new Student("Darius", "Navickas", 4),
            new Student("Katazyna", "Balcevic", 3),
            new Student("Justinas", "Karaliunas", 4),
            new Student("Andrius", "Gustas", 2)
    };

    System.out.println("Array before sorting:");
    System.out.println("-----");

    for (Student e : students) {
      System.out.println(e);
    }

    System.out.println("-----");

    Comparator<Student> compare = Comparator.comparing(Student::getClassRoom).thenComparing(Student::getLastName).thenComparing(Student::getFirstName);

    Arrays.sort(students, compare);

    System.out.println("Array after sorting:");

    System.out.println("-----");

    for (Student e : students) {
      System.out.println(e);
    }
  }
}