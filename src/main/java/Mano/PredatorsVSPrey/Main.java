package Mano.PredatorsVSPrey;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Main extends Application {

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws FileNotFoundException {
    Insets ins = new Insets(5, 5, 0, 5);
    FileInputStream imgsrc = new FileInputStream("C:\\Users\\JBKJu\\Desktop\\JAVA\\src\\Mano\\PredatorsVSPrey\\pvsp.png");
    Image img = new Image(imgsrc);
    BackgroundImage bgImg = new BackgroundImage(img,
            BackgroundRepeat.NO_REPEAT,
            BackgroundRepeat.NO_REPEAT,
            BackgroundPosition.CENTER,
            BackgroundSize.DEFAULT);
    Background bg = new Background(bgImg);

    primaryStage.setTitle("Predators VS Preys");
    primaryStage.setResizable(false);

    Group group = new Group();

    Label wolfLabel = new Label("Enter the number of wolves:");
    TextField wolfs = new TextField();
    wolfs.setMaxWidth(40);
    wolfs.setAlignment(Pos.CENTER);
    wolfLabel.setLabelFor(wolfs);
    wolfLabel.setStyle("-fx-background-color: rgba(255, 255, 255, 0.5);");
    wolfLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 15));

    HBox wBox = new HBox(20, wolfLabel, wolfs);
    wBox.setAlignment(Pos.CENTER);
    wBox.setPadding(ins);

    Label sheepLabel = new Label("Enter the number of sheeps:");
    TextField sheeps = new TextField();
    sheeps.setMaxWidth(40);
    sheeps.setAlignment(Pos.CENTER);
    sheepLabel.setLabelFor(sheeps);
    sheepLabel.setStyle("-fx-background-color: rgba(255, 255, 255, 0.5);");
    sheepLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 15));

    HBox sBox = new HBox(20, sheepLabel, sheeps);
    sBox.setAlignment(Pos.CENTER);
    sBox.setPadding(ins);

    Label gridLabel = new Label("ENTER GRID SIZE:");
    gridLabel.setTextFill(Paint.valueOf("White"));
    gridLabel.setStyle("-fx-background-color: rgba(0, 0, 0, 0.5);");
    gridLabel.setFont(Font.font("Verdana", FontWeight.EXTRA_BOLD, 20));

    Label xLabel = new Label("Rows:");
    TextField x = new TextField();
    x.setMaxWidth(40);
    x.setAlignment(Pos.CENTER);
    xLabel.setLabelFor(x);
    xLabel.setStyle("-fx-background-color: rgba(255, 255, 255, 0.5);");
    xLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 15));

    Label yLabel = new Label("Columns:");
    TextField y = new TextField();
    y.setMaxWidth(40);
    y.setAlignment(Pos.CENTER);
    yLabel.setLabelFor(y);
    yLabel.setStyle("-fx-background-color: rgba(255, 255, 255, 0.5);");
    yLabel.setFont(Font.font("Verdana", FontWeight.BOLD, 15));

    HBox xyBox = new HBox(20, xLabel, x, yLabel, y);
    xyBox.setAlignment(Pos.CENTER);
    xyBox.setPadding(ins);

    Button btn = new Button("Start Simulation");

    btn.setOnAction(e -> {
      Controller controller = new Controller();

      String wolfsCountString = wolfs.getText();
      String sheepsCountString = sheeps.getText();
      String xGridString = x.getText();
      String yGridString = y.getText();

      int wolfsCount = controller.checkInputs(wolfsCountString, "Wolves");
      int sheepsSount = controller.checkInputs(sheepsCountString, "Sheep");
      int xGrid = controller.checkInputs(xGridString, "Rows");
      int yGrid = controller.checkInputs(yGridString, "Columns");

      if ((wolfsCount + sheepsSount) > (xGrid * yGrid)) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Wrong Inputs");
        alert.setHeaderText("Too many animals");
        alert.setContentText("Enter lower number of animals (sheep or wolves) or bigger grid size(x or y).");
        alert.showAndWait();
      } else {
        try {
          controller.simulation(wolfsCount, sheepsSount, xGrid, yGrid);
          primaryStage.close();
        } catch (Exception e1) {
          e1.printStackTrace();
        }
      }
    });

    VBox vbox = new VBox(20, wBox, sBox, gridLabel, xyBox, btn);

    vbox.setAlignment(Pos.CENTER);
    vbox.setMinSize(330, 295);
    vbox.setBackground(bg);

    group.getChildren().add(vbox);

    Scene scene = new Scene(group, 320, 275);

    primaryStage.setScene(scene);

    primaryStage.show();
  }
}