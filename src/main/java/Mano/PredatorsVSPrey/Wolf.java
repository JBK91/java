package Mano.PredatorsVSPrey;

public class Wolf extends Animal {
  private int eaten = 0;

  Wolf(int type, int posX, int posY) {
    super(type, posX, posY);
    this.eaten = 0;
  }

  public int[] checkForPrey() {
    if (!bounds(this.getPosX() + 1, this.getPosY())) {
      if (getGrid()[this.getPosX() + 1][this.getPosY()] == 2) {
        return new int[]{this.getPosX() + 1, this.getPosY()};
      }
    }

    if (!bounds(this.getPosX() - 1, this.getPosY())) {
      if (getGrid()[this.getPosX() - 1][this.getPosY()] == 2) {
        return new int[]{this.getPosX() - 1, this.getPosY()};
      }
    }

    if (!bounds(this.getPosX(), this.getPosY() + 1)) {
      if (getGrid()[this.getPosX()][this.getPosY() + 1] == 2) {
        return new int[]{this.getPosX(), this.getPosY() + 1};
      }
    }

    if (!bounds(this.getPosX(), this.getPosY() - 1)) {
      if (getGrid()[this.getPosX()][this.getPosY() - 1] == 2) {
        return new int[]{this.getPosX(), this.getPosY() - 1};
      }
    }

    return new int[]{-1, -1};
  }

  public int getEaten() {
    return eaten;
  }

  public void setEaten(int eaten) {
    this.eaten = eaten;
  }

  public boolean starve() {
    return (this.getEaten() == 3);
  }

  @Override
  public boolean breed() {
    if (this.getSteps() >= 8) {
      this.setSteps(0);
      return true;
    }

    return false;
  }
}
