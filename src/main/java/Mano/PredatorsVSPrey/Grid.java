package Mano.PredatorsVSPrey;

class Grid {
  private static int width;
  private static int height;
  private static int[][] grid;

  Grid() {
  }

  Grid(int width, int height) {
    Grid.width = width;
    Grid.height = height;
    Grid.grid = new int[width][height];
  }

  public static int getWidth() {
    return width;
  }

  public static int getHeight() {
    return height;
  }

  public static int[][] getGrid() {
    return grid;
  }

  static void setGrid(int[][] grid) {
    Grid.grid = grid;
  }

  static boolean bounds(int x, int y) {
    return (x < 0 || y < 0 || y >= getHeight() || x >= getWidth());
  }

  public static boolean checkCell(int x, int y) {
    return (!bounds(x, y) && Grid.grid[x][y] == 0);
  }
}
