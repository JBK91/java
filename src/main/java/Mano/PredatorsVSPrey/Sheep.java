package Mano.PredatorsVSPrey;

public class Sheep extends Animal {
  public Sheep(int type, int posX, int posY) {
    super(type, posX, posY);
  }

  @Override
  public boolean breed() {
    if (this.getSteps() == 3) {
      this.setSteps(0);
      return true;
    }

    return false;
  }

  @Override
  public int[] checkForPrey() {
    return new int[]{-1, -1};
  }

  @Override
  public int getEaten() {
    return 0;
  }

  @Override
  public void setEaten(int eaten) {

  }

  @Override
  public boolean starve() {
    return false;
  }
}
