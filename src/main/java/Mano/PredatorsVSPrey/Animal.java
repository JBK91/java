package Mano.PredatorsVSPrey;

import java.util.Random;

abstract class Animal extends Grid {
  private static int wolfPopulation, sheepPopulation;
  private int type;
  private int posX;
  private int posY;
  private int steps;

  Animal(int type, int posX, int posY) {
    this.type = type;
    this.posX = posX;
    this.posY = posY;
    this.steps = 0;
  }

  public static int getSheepPopulation() {
    return sheepPopulation;
  }

  public static void setSheepPopulation(int sheepPopulation) {
    Animal.sheepPopulation = sheepPopulation;
  }

  public static int getWolfPopulation() {
    return wolfPopulation;
  }

  public static void setWolfPopulation(int wolfPopulation) {
    Animal.wolfPopulation = wolfPopulation;
  }

  public int getSteps() {
    return steps;
  }

  public void setSteps(int steps) {
    this.steps = steps;
  }

  int getPosX() {
    return posX;
  }

  public void setPosX(int posX) {
    this.posX = posX;
  }

  int getPosY() {
    return posY;
  }

  public void setPosY(int posY) {
    this.posY = posY;
  }

  int getType() {
    return type;
  }

  public void move() {
    Random random = new Random();

    int randomNumber = random.nextInt(4) + 1;

    switch (randomNumber) {
      case 1:
        // move top
        if (checkCell(this.getPosX(), this.getPosY() - 1)) {
          this.setPosY(this.getPosY() - 1);
        }
        break;
      case 2:
        // move right
        if (checkCell(this.getPosX() + 1, this.getPosY())) {
          this.setPosX(this.getPosX() + 1);
        }
        break;
      case 3:
        // move down
        if (checkCell(this.getPosX(), this.getPosY() + 1)) {
          this.setPosY(this.getPosY() + 1);
        }
        break;
      case 4:
        // move left
        if (checkCell(this.getPosX() - 1, this.getPosY())) {
          this.setPosX(this.getPosX() - 1);
        }
        break;
    }
  }

  public abstract boolean breed();

  public abstract int[] checkForPrey();

  public abstract int getEaten();

  public abstract void setEaten(int eaten);

  public abstract boolean starve();
}
