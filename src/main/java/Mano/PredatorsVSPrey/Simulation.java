package Mano.PredatorsVSPrey;

import javafx.animation.PauseTransition;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;

public class Simulation {
  static int steps = 0;
  static int wolvesDied = 0;
  static int ateSheep = 0;
  static int wolvesDieing = 0;
  static int newW = 0;
  static int newS = 0;
  static boolean isAuto = false;
  private int wolves;
  private int sheep;
  private int x;
  private int y;

  public Simulation(int wolves, int sheep, int x, int y) {
    this.wolves = wolves;
    this.sheep = sheep;
    this.x = x;
    this.y = y;
  }

  private static void print(GridPane gridPane, HBox info, ArrayList<Animal> animals, double screenHeight) throws FileNotFoundException {
    gridPane.getChildren().clear();
    info.getChildren().clear();
    double prefSize = (screenHeight - 70.0D) / (double) Grid.getHeight();
    Label population = new Label("Population: " + (Animal.getWolfPopulation() + Animal.getSheepPopulation()));
    Label wolfPopulation = new Label("Wolves: " + Animal.getWolfPopulation());
    Label sheepPopulation = new Label("Sheep: " + Animal.getSheepPopulation());
    Label stepsLabel = new Label("Steps: " + steps);
    VBox pop = new VBox(new Node[]{population, wolfPopulation, sheepPopulation, stepsLabel});
    pop.setAlignment(Pos.CENTER_LEFT);
    Label prevStep = new Label("PREVIOUS STEP RESULT:");
    Label eatedSheepLabel = new Label("Sheep had ate: " + ateSheep);
    Label wolvesDiedLabel = new Label("Number of died wolves: " + wolvesDied);
    Label wolvesDieingLabel = new Label("Wolves will die at next step: " + wolvesDieing);
    VBox step = new VBox(new Node[]{prevStep, eatedSheepLabel, wolvesDiedLabel, wolvesDieingLabel});
    step.setAlignment(Pos.CENTER_LEFT);
    Label newWolvesLabel = new Label("New wolves: " + newW);
    Label newSheepLabel = new Label("New sheep: " + newS);
    VBox newA = new VBox(new Node[]{newSheepLabel, newWolvesLabel});
    newA.setAlignment(Pos.CENTER);
    Button nextBtn = new Button("Next");
    nextBtn.setAlignment(Pos.CENTER);
    nextBtn.setOnAction((e) -> {
      makeMove(animals);

      try {
        print(gridPane, info, animals, screenHeight);
      } catch (FileNotFoundException var7) {
        var7.printStackTrace();
      }

    });
    Button autoBtn = new Button("Auto");
    autoBtn.setAlignment(Pos.CENTER);
    if (isAuto) {
      nextBtn.setDisable(true);
    }

    autoBtn.setOnAction((e) -> {
      nextBtn.setDisable(true);
      PauseTransition wait = new PauseTransition(Duration.seconds(0.2D));
      wait.setOnFinished((el) -> {
        nextBtn.setDisable(true);
        if (isAnimal()) {
          isAuto = true;
          makeMove(animals);

          try {
            print(gridPane, info, animals, screenHeight);
          } catch (FileNotFoundException var9) {
            var9.printStackTrace();
          }

          wait.playFromStart();
        }

      });
      wait.play();
    });
    Button endBtn = new Button("End");
    endBtn.setAlignment(Pos.CENTER);
    endBtn.setOnAction((e) -> {
      while (isAnimal()) {
        makeMove(animals);
      }

      try {
        print(gridPane, info, animals, screenHeight);
      } catch (FileNotFoundException var7) {
        var7.printStackTrace();
      }

    });
    if (!isAnimal()) {
      nextBtn.setDisable(true);
      autoBtn.setDisable(true);
      endBtn.setDisable(true);
    }

    HBox btnBox = new HBox(new Node[]{nextBtn, autoBtn, endBtn});
    btnBox.setAlignment(Pos.CENTER);
    info.getChildren().addAll(new Node[]{pop, step, newA, btnBox});
    info.setSpacing(30.0D);
    info.setMaxHeight(40.0D);
    FileInputStream src1 = new FileInputStream("C:\\Users\\JBKJu\\Desktop\\JAVA\\src\\Mano\\PredatorsVSPrey\\wolf.png");
    Image wolfImg = new Image(src1);
    FileInputStream src2 = new FileInputStream("C:\\Users\\JBKJu\\Desktop\\JAVA\\src\\Mano\\PredatorsVSPrey\\sheep.png");
    Image sheepImg = new Image(src2);

    for (int i = 0; i < Grid.getGrid().length; ++i) {
      for (int j = 0; j < Grid.getGrid()[i].length; ++j) {
        Label label = new Label("");
        ImageView sheepp;
        int sheepIndex;
        Tooltip tt;
        if (Grid.getGrid()[i][j] == 1) {
          sheepp = new ImageView(wolfImg);
          sheepp.setFitHeight(prefSize);
          sheepp.setFitWidth(prefSize);
          sheepIndex = getIndex(1, i, j, animals);
          tt = new Tooltip("Steps: " + (animals.get(sheepIndex)).getSteps() + "\nAte before: " + (animals.get(sheepIndex)).getEaten() + " steps");
          label = new Label("", sheepp);
          sheepp = null;
          label.setTooltip(tt);
        } else if (Grid.getGrid()[i][j] == 2) {
          sheepp = new ImageView(sheepImg);
          sheepp.setFitWidth(prefSize);
          sheepp.setFitHeight(prefSize);
          sheepIndex = getIndex(2, i, j, animals);
          tt = new Tooltip("Steps: " + (animals.get(sheepIndex)).getSteps());
          label = new Label("", sheepp);
          sheepp = null;
          label.setTooltip(tt);
        }

        label.setMinSize(prefSize, prefSize);
        label.setMaxSize(prefSize, prefSize);
        label.setAlignment(Pos.CENTER);
        label.setBorder(new Border(new BorderStroke[]{new BorderStroke(Paint.valueOf("Black"), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)}));
        gridPane.add(label, i, j);
      }
    }

  }

  private static boolean isAnimal() {
    return Animal.getSheepPopulation() != 0 && Animal.getWolfPopulation() != 0;
  }

  private static int getIndex(int t, int x, int y, ArrayList<Animal> animals) {
    int index = -1;

    for (int i = 0; i < animals.size(); ++i) {
      if ((animals.get(i)).getType() == t && (animals.get(i)).getPosX() == x && (animals.get(i)).getPosY() == y) {
        index = i;
      }
    }

    return index;
  }

  private static int[] checkCells(int x, int y) {
    if (Grid.checkCell(x + 1, y)) {
      return new int[]{x + 1, y};
    } else if (Grid.checkCell(x - 1, y)) {
      return new int[]{x - 1, y};
    } else if (Grid.checkCell(x, y + 1)) {
      return new int[]{x, y + 1};
    } else {
      return Grid.checkCell(x, y - 1) ? new int[]{x, y - 1} : new int[]{-1, -1};
    }
  }

  private static int[] getRandomIndexes() {
    Random random = new Random();
    int randomX = random.nextInt(Grid.getWidth());
    int randomY = random.nextInt(Grid.getHeight());
    if (Grid.checkCell(randomX, randomY)) {
      int[][] newGrid = Grid.getGrid();
      newGrid[randomX][randomY] = 3;
      Grid.setGrid(newGrid);
      return new int[]{randomX, randomY};
    } else {
      return getRandomIndexes();
    }
  }

  public static void makeMove(ArrayList<Animal> animals) {
    ArrayList<Animal> newWolfs = new ArrayList();
    ArrayList<Animal> rAnimal = new ArrayList();
    ++steps;
    ateSheep = 0;
    wolvesDied = 0;
    wolvesDieing = 0;
    newW = 0;
    newS = 0;

    for (int j = 0; j < animals.size(); ++j) {
      if ((animals.get(j)).getType() == 1) {
        int[][] ant = Grid.getGrid();
        ant[(animals.get(j)).getPosX()][(animals.get(j)).getPosY()] = 0;
        int[] isPrey = (animals.get(j)).checkForPrey();
        if (isPrey[0] == -1 && isPrey[1] == -1) {
          (animals.get(j)).move();
          ant[(animals.get(j)).getPosX()][(animals.get(j)).getPosY()] = 1;
          (animals.get(j)).setSteps((animals.get(j)).getSteps() + 1);
          (animals.get(j)).setEaten((animals.get(j)).getEaten() + 1);
        } else {
          ant[isPrey[0]][isPrey[1]] = 1;
          (animals.get(j)).setPosX(isPrey[0]);
          (animals.get(j)).setPosY(isPrey[1]);
          (animals.get(j)).setSteps((animals.get(j)).getSteps() + 1);
          (animals.get(j)).setEaten(0);
          ++ateSheep;
          rAnimal.add(new Sheep(2, (animals.get(j)).getPosX(), (animals.get(j)).getPosY()));
        }

        Grid.setGrid(ant);
        if ((animals.get(j)).breed()) {
          int[] newSheep = checkCells((animals.get(j)).getPosX(), (animals.get(j)).getPosY());
          if (newSheep[0] != -1 && newSheep[1] != -1) {
            newWolfs.add(new Wolf(1, newSheep[0], newSheep[1]));
            ant[newSheep[0]][newSheep[1]] = 1;
          }
        }

        Grid.setGrid(ant);
        if ((animals.get(j)).starve()) {
          (animals.get(j)).setEaten(0);
          ant[(animals.get(j)).getPosX()][(animals.get(j)).getPosY()] = 0;
          ++wolvesDied;
          rAnimal.add(new Wolf(1, (animals.get(j)).getPosX(), (animals.get(j)).getPosY()));
        }

        Grid.setGrid(ant);
      }
    }

    for (Animal r : animals) {
      if (r.getType() == 1 && r.getEaten() == 2) {
        ++wolvesDieing;
      }
    }

    for (Animal r : animals) {
      if (r.getType() == 1) {
        Animal.setWolfPopulation(Animal.getWolfPopulation() - 1);
      }

      if (r.getType() == 2) {
        Animal.setSheepPopulation(Animal.getSheepPopulation() - 1);
      }

      int ri = getIndex(r.getType(), r.getPosX(), r.getPosY(), animals);
      animals.remove(ri);
    }


    for (Animal r : animals) {
      animals.add(new Wolf(1, r.getPosX(), r.getPosY()));
      ++newW;
      Animal.setWolfPopulation(Animal.getWolfPopulation() + 1);
    }

    ArrayList<Animal> newSheeps = new ArrayList();

    for (int k = 0; k < animals.size(); ++k) {
      if ((animals.get(k)).getType() == 2) {
        int[][] ant = Grid.getGrid();
        ant[(animals.get(k)).getPosX()][(animals.get(k)).getPosY()] = 0;
        (animals.get(k)).move();
        ant[(animals.get(k)).getPosX()][(animals.get(k)).getPosY()] = 2;
        (animals.get(k)).setSteps((animals.get(k)).getSteps() + 1);
        Grid.setGrid(ant);
        if ((animals.get(k)).breed()) {
          int[] newSheep = checkCells((animals.get(k)).getPosX(), (animals.get(k)).getPosY());
          if (newSheep[0] != -1 && newSheep[1] != -1) {
            newSheeps.add(new Sheep(2, newSheep[0], newSheep[1]));
            ant[newSheep[0]][newSheep[1]] = 2;
          }
        }

        Grid.setGrid(ant);
      }
    }

    for (Animal ani : newSheeps) {
      animals.add(new Sheep(2, ani.getPosX(), ani.getPosY()));
      ++newS;
      Animal.setSheepPopulation(Animal.getSheepPopulation() + 1);
    }

  }

  public void startSimulation() throws FileNotFoundException {
    Stage stage = new Stage();
    stage.setTitle("Predators VS Preys");
    stage.sizeToScene();
    stage.setResizable(false);
    Rectangle2D size = Screen.getPrimary().getVisualBounds();
    double screenHeight = size.getHeight() - 40.0D;
    GridPane gridPane = new GridPane();
    gridPane.setStyle("-fx-background-color: white;");
    new Grid(this.x, this.y);
    ArrayList<Animal> animals = this.populate();
    HBox info = new HBox();
    print(gridPane, info, animals, screenHeight);
    VBox infoBox = new VBox(new Node[]{info, gridPane});
    stage.setScene(new Scene(infoBox, screenHeight - 70.0D, screenHeight));
    stage.show();
  }

  private ArrayList<Animal> populate() {
    ArrayList<Animal> animals = new ArrayList();


    int[] randomIndexes;
    for (int i = 0; i < this.sheep; ++i) {
      randomIndexes = getRandomIndexes();
      animals.add(new Sheep(2, randomIndexes[0], randomIndexes[1]));
    }

    for (int i = 0; i < this.wolves; ++i) {
      randomIndexes = getRandomIndexes();
      animals.add(new Wolf(1, randomIndexes[0], randomIndexes[1]));
    }

    for (Animal t : animals) {
      if (t.getType() == 1) {
        Animal.setWolfPopulation(Animal.getWolfPopulation() + 1);
      }

      if (t.getType() == 2) {
        Animal.setSheepPopulation(Animal.getSheepPopulation() + 1);
      }
    }

    int[][] an = Grid.getGrid();

    for (Animal t : animals) {
      an[t.getPosX()][t.getPosY()] = t.getType();
    }

    Grid.setGrid(an);
    return animals;
  }

  public int getWolves() {
    return this.wolves;
  }

  public int getSheep() {
    return this.sheep;
  }

  public int getX() {
    return this.x;
  }

  public int getY() {
    return this.y;
  }
}
