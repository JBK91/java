package Mano.PredatorsVSPrey;

import javafx.scene.control.Alert;

import java.io.FileNotFoundException;


public class Controller {
  public int checkInputs(String check, String message) {
    if (check.equals("")) {
      Alert alert = new Alert(Alert.AlertType.ERROR);
      alert.setTitle("Missing input");
      alert.setHeaderText("Missing Number of " + message);
      alert.setContentText("Enter Number of " + message);
      alert.showAndWait();
    } else {
      try {
        return Integer.parseInt(check);
      } catch (NumberFormatException e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Wrong Format");
        alert.setHeaderText("Wrong Number of " + message + " Input Value");
        alert.setContentText("Enter Correct Number of " + message + " Value");
        alert.showAndWait();
      }
    }

    return -1;
  }

  public void simulation(int wolves, int sheeps, int x, int y) throws FileNotFoundException {
    Simulation sim = new Simulation(wolves, sheeps, x, y);

    sim.startSimulation();
  }
}
