package Udemy.List.BoxingChallenge;

import java.util.ArrayList;
import java.util.List;

public class Bank {
  private String name;
  private List<Branch> branches;

  public Bank(String name) {
    this.name = name;
    this.branches = new ArrayList<>();
  }

  public void addBranch(String name) {
    if (branchExist(name)) {
      System.out.println("Branch already exists");
      return;
    }

    this.branches.add(new Branch(name));
    System.out.println("Branch " + name + " added");
  }

  public void addCustomer(String bName, String name, double amount) {
    if (!branchExist(bName)) {
      System.out.println("Branch don't exist");
      return;
    }

    int index = getIndex(bName);

    this.branches.get(index).addCustomer(name, amount);
  }

  public void addTransaction(String bName, String name, double amount) {
    if (!branchExist(bName)) {
      System.out.println("Branch don't exist");
      return;
    }

    int index = getIndex(bName);

    this.branches.get(index).addTransaction(name, amount);
  }

  public void printBranch(String bName) {
    if (!branchExist(bName)) {
      System.out.println("Branch don't exist");
      return;
    }

    int index = getIndex(bName);

    this.branches.get(index).printCustomers();
  }

  private int getIndex(String name) {
    for (int i = 0; i < this.branches.size(); ++i) {
      if (this.branches.get(i).getName().equals(name)) return i;
    }

    return -1;
  }

  private boolean branchExist(String name) {
    for (Branch b : branches) {
      if (b.getName().equals(name)) return true;
    }

    return false;
  }
}
