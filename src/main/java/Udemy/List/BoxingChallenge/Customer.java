package Udemy.List.BoxingChallenge;

import java.util.ArrayList;
import java.util.List;

public class Customer {
  private String name;
  private List<Double> transactions;

  public Customer(String name, double amount) {
    this.name = name;
    this.transactions = new ArrayList<>();
    addTransaction(amount);
  }

  public void addTransaction(double amount) {
    this.transactions.add(amount);

    System.out.println("Transaction successful");
  }

  public String printTransactions() {
    StringBuilder rez = new StringBuilder();
    for (double t : this.transactions) {
      rez.append(" ").append(t);
    }

    return rez.toString();
  }

  public String getName() {
    return name;
  }

  public List<Double> getTransactions() {
    return transactions;
  }
}
