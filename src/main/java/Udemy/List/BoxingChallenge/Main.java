package Udemy.List.BoxingChallenge;

public class Main {
  public static void main(String[] args) {
    Bank bank = new Bank("SEB");
    bank.addBranch("Privatus");
    bank.addCustomer("Privatus", "Justinas", 50);
    bank.addTransaction("Privatus", "Justinas", 40);
    bank.printBranch("Privatus");
  }
}
