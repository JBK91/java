package Udemy.List.BoxingChallenge;

import java.util.ArrayList;
import java.util.List;

public class Branch {
  private String name;
  private List<Customer> customers;

  public Branch(String name) {
    this.name = name;
    this.customers = new ArrayList<>();
  }

  public void addCustomer(String name, double amount) {
    if (customerExist(name)) {
      System.out.println("Customer already exists");
      return;
    }

    this.customers.add(new Customer(name, amount));

    System.out.println("Customer added to " + this.name + " branch");
  }

  public void addTransaction(String name, double amount) {
    if (!customerExist(name)) {
      System.out.println("Customer don't exists");
      return;
    }

    int index = getIndex(name);

    customers.get(index).addTransaction(amount);

    System.out.println("Amount added");
  }

  public void printCustomers() {
    for (Customer c : customers) {
      System.out.println(c.getName() + "" + c.printTransactions());
    }
  }

  private int getIndex(String name) {
    for (int i = 0; i < this.customers.size(); ++i) {
      if (customers.get(i).getName().equals(name)) return i;
    }

    return -1;
  }

  private boolean customerExist(String name) {
    for (Customer c : this.customers) {
      if (c.getName().equals(name)) return true;
    }

    return false;
  }

  public String getName() {
    return name;
  }

  public List<Customer> getCustomers() {
    return customers;
  }
}
