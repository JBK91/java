package Udemy.List.LinkedListChallenge;

import java.util.ArrayList;
import java.util.List;

public class Album {
  private String title;
  private List<Song> songs;

  public Album(String title, Song song) {
    this.title = title;
    this.songs = new ArrayList<>();
    addSong(song);
  }

  public String getTitle() {
    return title;
  }

  public List<Song> getSongs() {
    return songs;
  }

  public void addSong(Song song) {
    if (findSong(song)) {
      System.out.println("Song already exists in this album");
    } else {
      this.songs.add(song);
    }
  }

  public boolean findSong(Song song) {
    for (Song s : this.songs) {
      if (s.getTitle().equals(song.getTitle())) return true;
    }

    return false;
  }
}
