package Udemy.List.LinkedListChallenge;

import java.util.ListIterator;
import java.util.Scanner;

public class Main {
  public static void main(String[] args) {
    Playlist pl = new Playlist();

    pl.addAlbum(new Album("Rock", new Song("I Rock You", 4.54)));

    pl.addSong("I Rock You");

    Song s1 = new Song("Lollipop", 2.59);
    Song s2 = new Song("BuuBuu", 3.48);
    Song s3 = new Song("Brock", 5.05);

    Album a = new Album("Life", new Song("Kol", 1.25));

    a.addSong(s1);
    a.addSong(s2);
    a.addSong(s3);

    pl.addAlbum(a);

    pl.addSong("Lollipop");
    pl.addSong("BuuBuu");
    pl.addSong("Brock");
    pl.addSong("Kol");

    showMenu(pl);
  }

  private static void showMenu(Playlist pl) {
    Scanner scanner = new Scanner(System.in);

    ListIterator<Song> li = pl.getSongs().listIterator();

    if (li.hasNext()) System.out.println("Now playing " + li.next().getTitle());

    System.out.println("Actions:");
    System.out.println("\t0 - Exit");
    System.out.println("\t1 - Next song");
    System.out.println("\t2 - Previous song");
    System.out.println("\t3 - Repeat this song");
    System.out.println("\t4 - Remove this song from playlist");

    boolean exit = true;
    boolean gf = true;

    while (exit) {
      int action = scanner.nextInt();

      switch (action) {
        case 0:
          exit = false;
          break;
        case 1:
          if (li.hasNext()) {
            if (!gf) {
              gf = true;
            }

            System.out.println("Now playing " + li.next().getTitle());
          } else {
            System.out.println("End");
          }

          break;
        case 2:
          if (li.hasPrevious()) {
            if (gf) {
              gf = false;
              li.previous();
            }

            System.out.println("Now playing " + li.previous().getTitle());
          } else {
            System.out.println("Begin");
          }

          break;
        case 3:
          if (li.hasPrevious()) {
            System.out.println("Now playing " + li.previous().getTitle());
          } else {
            System.out.println("Begin");
          }
          break;
        case 4:
          if (li.hasPrevious()) li.remove();
          break;
      }
    }
  }
}
