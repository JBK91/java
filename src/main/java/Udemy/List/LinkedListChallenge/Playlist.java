package Udemy.List.LinkedListChallenge;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Playlist {
  private List<Album> albums;
  private List<Song> songs;

  public Playlist() {
    this.albums = new ArrayList<>();
    this.songs = new LinkedList<>();
  }

  public void addAlbum(Album album) {
    if (findAlbum(album)) {
      System.out.println("Album already exists");
    } else {
      this.albums.add(album);
    }
  }

  public void addSong(String song) {
    int albumIndex = findAlbumIndex(song);

    if (albumIndex == -1) {
      System.out.println("Song didn't exists");
    } else {
      int songIndex = findSongIndex(song);
      this.songs.add(this.albums.get(albumIndex).getSongs().get(songIndex));
    }
  }

  private boolean findAlbum(Album album) {
    for (Album a : this.albums) {
      if (album.getTitle().equals(a.getTitle())) return true;
    }

    return false;
  }

  private int findAlbumIndex(String song) {
    for (int i = 0; i < this.albums.size(); ++i) {
      for (int j = 0; j < this.albums.get(i).getSongs().size(); ++j) {
        if (song.equals(this.albums.get(i).getSongs().get(j).getTitle())) return i;
      }
    }

    return -1;
  }

  private int findSongIndex(String song) {
    for (int i = 0; i < this.albums.size(); ++i) {
      for (int j = 0; j < this.albums.get(i).getSongs().size(); ++j) {
        if (song.equals(this.albums.get(i).getSongs().get(j).getTitle())) return j;
      }
    }

    return -1;
  }

  public List<Song> getSongs() {
    return songs;
  }
}
