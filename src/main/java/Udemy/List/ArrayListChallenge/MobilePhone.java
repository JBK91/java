package Udemy.List.ArrayListChallenge;

import java.util.ArrayList;
import java.util.List;

public class MobilePhone {
  private List<Contact> contacts;

  public MobilePhone() {
    this.contacts = new ArrayList<>();
  }

  public void printContacts() {
    System.out.println("Your contacts:");
    for (Contact c : contacts) {
      System.out.println(c);
    }
  }

  public void addContact(String name, String phoneNumber) {
    if (!isContactExists(name)) {
      contacts.add(new Contact(name, phoneNumber));
      System.out.println("Contact added");
    } else {
      System.out.println("Contact already exists");
    }
  }

  public void updateContact(String name, String newName, String newPhoneNumber) {
    if (isContactExists(name)) {
      if (newName != null && newPhoneNumber != null) {
        updateAll(name, newName, newPhoneNumber);
        System.out.println("Contact updated.");
      } else if (newName != null) {
        updateName(name, newName);
        System.out.println("Contact updated.");
      } else if (newPhoneNumber != null) {
        updatePhoneNumber(name, newPhoneNumber);
        System.out.println("Contact updated.");
      } else {
        System.out.println("Enter new name or new phone number!");
      }
    } else {
      System.out.println("Contact don't exist in your contact list!");
    }
  }

  public void removeContact(String name) {
    if (isContactExists(name)) {
      int index = getIndex(name);

      contacts.remove(index);

      System.out.println("Contact deleted");
    } else {
      System.out.println("Contact don't exist in your contact list!");
    }
  }

  public void findContact(String name) {
    if (isContactExists(name)) {
      int index = getIndex(name);

      System.out.println("Contact found:");
      System.out.println(contacts.get(index).getName() + " " + contacts.get(index).getPhoneNumber());
    } else {
      System.out.println("Contact don't exist in your contact list!");
    }
  }

  private void updateAll(String name, String newName, String newPhone) {
    int index = getIndex(name);

    updateName(name, newName);
    updatePhoneNumber(name, newPhone);
  }

  private void updateName(String name, String newName) {
    int index = getIndex(name);

    contacts.get(index).setName(newName);
  }

  private void updatePhoneNumber(String name, String newPhone) {
    int index = getIndex(name);

    contacts.get(index).setPhoneNumber(newPhone);
  }

  private int getIndex(String name) {
    for (int i = 0; i < contacts.size(); ++i) {
      if (contacts.get(i).getName().equals(name)) return i;
    }

    return -1;
  }

  private boolean isContactExists(String name) {
    for (Contact c : contacts) {
      if (c.getName().equals(name)) return true;
    }

    return false;
  }
}
