package Udemy.List.ArrayListChallenge;

import java.util.Scanner;

public class Main {
  private static Scanner scanner = new Scanner(System.in);
  private static MobilePhone contactList = new MobilePhone();

  public static void main(String[] args) {
    showDialog();
  }

  private static void showDialog() {
    System.out.println("\nMenu:");
    System.out.println("\t1 - Print contacts");
    System.out.println("\t2 - Add new contact");
    System.out.println("\t3 - Modify contact");
    System.out.println("\t4 - Remove contact");
    System.out.println("\t5 - Search contact");
    System.out.println("\t6 - Exit");

    int action = scanner.nextInt();

    switch (action) {
      case 1:
        contactList.printContacts();
        showDialog();
        break;
      case 2:
        addNewContact();
        break;
      case 3:
        modifyContact();
        break;
      case 4:
        removeContact();
        break;
      case 5:
        searchContact();
      case 6:
        System.exit(0);
        break;
      default:
        showDialog();
    }
  }

  private static void addNewContact() {
    System.out.print("Enter name: ");

    String name = readFromConsole();

    System.out.print("Enter mobile phone: ");

    String phone = readFromConsole();

    contactList.addContact(name, phone);

    showDialog();
  }

  private static void modifyContact() {
    System.out.print("Enter existing contact name: ");

    String name = readFromConsole();

    System.out.println("What you want to modify:");
    System.out.println("\t1 - Name");
    System.out.println("\t2 - Phone");
    System.out.println("\t3 - Both");

    int action = scanner.nextInt();

    String newName = null;
    String newPhone = null;

    switch (action) {
      case 1:
        System.out.print("Enter name: ");
        newName = readFromConsole();
        break;
      case 2:
        System.out.print("Enter phone: ");
        newPhone = readFromConsole();
        break;
      case 3:
        System.out.print("Enter name: ");
        newName = readFromConsole();
        System.out.print("Enter phone: ");
        newPhone = readFromConsole();
        break;
      default:
        System.out.println("Wrong option. Returning..");
        showDialog();
        break;
    }

    contactList.updateContact(name, newName, newPhone);
    showDialog();
  }

  private static void removeContact() {
    System.out.print("Enter name: ");

    String name = readFromConsole();

    contactList.removeContact(name);
    showDialog();
  }

  private static void searchContact() {
    System.out.print("Enter name: ");

    String name = readFromConsole();

    contactList.findContact(name);
    showDialog();
  }

  private static String readFromConsole() {
    return scanner.next();
  }
}
